# Mindgaze.AspNetCore

[![pipeline status](https://gitlab.com/afivan/mindgaze-aspnetcore/badges/master/pipeline.svg)](https://gitlab.com/afivan/mindgaze-aspnetcore/-/commits/master)

General multipurpose library for rapid app development with ASP.NET Core.

## About

By using this battle tested library, you can benefit from a more rapid application development, API results pagination, ordering, filtering, base model and DTO classes, extension methods and integration test fixtures. JsonPatch is also supported.

## Installation

You can find this library on [NuGet](https://www.nuget.org/packages/Mindgaze.AspNetCore/), hence you can install it from Visual Studio Package Manager, modify the csproj file or run this command:

`$ dotnet add package Mindgaze.AspNetCore`

## Usage

Once you install it in your project, all the features can be found in Mindgaze.AspNetCore.* namespace. Depending on what you want to achieve, you can find articles on the [blog](https://blog.mindgaze.tech/category/development/aspnetcore-library/). New articles are constantly added explaining the features.

Start with this [article](https://blog.mindgaze.tech/2019/12/19/mindgaze-aspnetcore-library-apis-with-ease/) to have your project setup and become familiar with the library. There are many more so please check on the blog and [subscribe to our newsletter](https://www.mindgaze.tech/Home/NewsletterSubscribe?utm_source=gitlab).

## Benefits

We are using this application in all of our APIs made with .NET Core. Not only the development becomes easier, but you can also extend functionality to accommodate business logic needs etc. Features for testing enable us to implement integration testing to ensure the software works fine reducing the number of bugs in production environment. Having DTOs supplied with DI services makes it easy to prepopulate fields with values let's say from database before validation occurs. This way we keep the controller clean and easy to maintain.

## Contributions & issues

Any contribution is welcome! Make sure you work in a separate branch like dev-contributor and submit a merge request
If you find a bug, I'd be more than happy to assist you! Give me as many repro details as possible so I can have a look.
Don't forget, use dark themes when developing to attract less bugs ;)



