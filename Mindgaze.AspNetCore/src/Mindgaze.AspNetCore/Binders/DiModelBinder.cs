﻿using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Mindgaze.AspNetCore.Dto;
using Mindgaze.AspNetCore.DtoUtils;
using Mindgaze.AspNetCore.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Binders
{
    public class DiModelBinder : IModelBinder
    {
        private readonly IList<Microsoft.AspNetCore.Mvc.Formatters.IInputFormatter> Formatters;

        public DiModelBinder(IList<Microsoft.AspNetCore.Mvc.Formatters.IInputFormatter> formatters)
        {
            Formatters = formatters;
        }

        public async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var services = bindingContext.HttpContext.RequestServices;

            IHttpRequestStreamReaderFactory httpRequestStreamReaderFactory =
                services.GetService(typeof(IHttpRequestStreamReaderFactory)) as IHttpRequestStreamReaderFactory;

            var bodyBinder = new BodyModelBinder(Formatters, httpRequestStreamReaderFactory);

            await bodyBinder.BindModelAsync(bindingContext);

            var argument = bindingContext.Result.Model;

            if (argument != null)
            {
                SupplyInterfaces(argument, services);
                SupplyToProperties(argument, services);
            }
        }

        internal static void SupplyInterfaces(object argument, IServiceProvider services)
        {
            var supplyInterfaces = argument.GetType().GetInterfaces()
                    .Where(i => i.IsDependencyDtoServiceSupplier())
                    ;

            foreach (var supplyInterface in supplyInterfaces)
            {
                var genericArgumentTypes = supplyInterface.GetGenericArguments();
                var resolvedValues = genericArgumentTypes
                    .Select(genericType => services.GetService(genericType))
                    .ToArray();

                var supplyMethodInfo = argument.GetType().GetMethod(nameof(IDependencyDtoServiceSupplier<object>.SupplyServices), genericArgumentTypes);

                supplyMethodInfo.Invoke(argument, resolvedValues);
            }
        }

        internal static void SupplyToProperties(object argument, IServiceProvider services)
        {
            var type = argument.GetType();

            if (argument is IEnumerable<object> enumerableValues)
            {
                foreach (var collectionValue in enumerableValues)
                {
                    SupplyInterfaces(collectionValue, services);
                    SupplyToProperties(collectionValue, services);
                }
            }
            else
            {
                foreach (var property in type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    .Where(p => (p.PropertyType.IsClass &&
                        p.PropertyType.BaseType.IsGenericType && 
                        p.PropertyType.BaseType.GetGenericTypeDefinition()
                            .IsAssignableFrom(typeof(ModelDto<>))) ||
                        (p.PropertyType.IsGenericType &&
                        p.PropertyType.GetGenericTypeDefinition().IsAssignableToGenericType(typeof(IEnumerable<>)) &&
                        p.PropertyType.GetGenericArguments()[0].IsAssignableToGenericType(typeof(ModelDto<>)))
                        )
                    )
                {
                    var traversedValue = property.GetValue(argument);

                    if (traversedValue != null)
                    {
                        SupplyInterfaces(traversedValue, services);
                        SupplyToProperties(traversedValue, services);
                    }
                }
            }
        }
    }
}
