﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Mindgaze.AspNetCore.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mindgaze.AspNetCore.Binders
{
    public class DiModelBinderProvider : IModelBinderProvider
    {
        private readonly IList<Microsoft.AspNetCore.Mvc.Formatters.IInputFormatter> Formatters;

        public DiModelBinderProvider(IList<Microsoft.AspNetCore.Mvc.Formatters.IInputFormatter> formatters)
        {
            this.Formatters = formatters;
        }

        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null) { throw new ArgumentNullException(nameof(context)); }

            if (context.Metadata.IsComplexType &&
                !context.Metadata.IsCollectionType &&
                (context.Metadata.ModelType.HasDependencyDtoServiceSupplierImplemented() || context.Metadata.ModelType.HasDependencyDtoServiceSupplierImplementedByAnyProperty()) &&
                (context.BindingInfo?.BindingSource?.CanAcceptDataFrom(BindingSource.Body) ?? false)
                )
            {
                var propertyBinders = context.Metadata.Properties.ToDictionary(property => property, context.CreateBinder);
                return new DiModelBinder(Formatters);
            }

            return null;
        }
    }
}
