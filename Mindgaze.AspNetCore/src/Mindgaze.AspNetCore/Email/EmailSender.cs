﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Mindgaze.AspNetCore.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Email
{
    class EmailSender : IEmailSender
    {
        private readonly EmailConfiguration _emailConfiguration;
        private readonly SmtpClient _smtpClient;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<EmailSender> _logger;

        private HttpClient GetClient()
        {
            HttpClient client;

            if (string.IsNullOrEmpty(_emailConfiguration.HttpClientName))
            {
                client = _httpClientFactory.CreateClient();
            }
            else
            {
                client = _httpClientFactory.CreateClient(_emailConfiguration.HttpClientName);
            }

            client.BaseAddress = new Uri(_emailConfiguration.Template.TemplateServiceUrl);

            return client;
        }

        private async Task<HttpResponseMessage> GetResponseAsync(EmailModel emailModel, string templateViewName)
        {
            var client = GetClient();

            var content = new StringContent(JsonConvert.SerializeObject(emailModel), Encoding.UTF8, "application/json");
            return await client.PostAsync($"{_emailConfiguration.Template.TemplateController}/{templateViewName}", content);
        }

        public EmailSender(IOptions<EmailConfiguration> emailOptions, IHttpClientFactory httpClientFactory, ILogger<EmailSender> logger)
        {
            _emailConfiguration = emailOptions.Value;
            _httpClientFactory = httpClientFactory;
            _logger = logger;

            _smtpClient = new SmtpClient()
            {
                Host = _emailConfiguration.Host,
                Port = _emailConfiguration.Port,
                EnableSsl = _emailConfiguration.EnableSsl,
            };

            if (!string.IsNullOrEmpty(_emailConfiguration.Username))
            {
                _smtpClient.Credentials = new NetworkCredential(_emailConfiguration.Username, _emailConfiguration.Password);
            }
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var mailMessage = new MailMessage(_emailConfiguration.Sender, email, subject, message)
            {
                IsBodyHtml = true,
            };
            
            await _smtpClient.SendMailAsync(mailMessage);
        }

        public async Task SendEmailAsync(IReadOnlyCollection<string> emailAddresses, string subject, string message)
        {
            foreach (var email in emailAddresses)
            {
                await SendEmailAsync(email, subject, message);
            }
        }

        public async Task<string> RenderTemplatedEmailAsync(EmailModel emailModel, string templateViewName)
        {
            var resp = await GetResponseAsync(emailModel, templateViewName);
            var body = await resp.Content.ReadAsStringAsync();

            if (!resp.IsSuccessStatusCode)
            {
                return $"<h1>Error rendering the template: {body}</h1>";
            }

            return body;
        }

        public async Task SendTemplatedEmailAsync(EmailModel emailModel, string templateViewName)
        {
            var resp = await GetResponseAsync(emailModel, templateViewName);
            var body = await resp.Content.ReadAsStringAsync();

            if (!resp.IsSuccessStatusCode)
            {
                _logger.LogError($"Could not send email for template {templateViewName}: {resp.StatusCode}-{emailModel.ToEmail} {body}");
            }
            else
            {
                await SendEmailAsync(emailModel.ToEmail, emailModel.Subject, body);
            }
        }

        public async Task SendTemplatedEmailAsync(IReadOnlyCollection<EmailModel> emailModels, string templateViewName)
        {
            foreach(var model in emailModels)
            {
                await SendTemplatedEmailAsync(model, templateViewName);
            }
        }

        public async Task SendTemplatedEmailAsync(IReadOnlyCollection<string> emailAddresses, EmailModel emailModel, string templateViewName)
        {
            var resp = await GetResponseAsync(emailModel, templateViewName);
            var body = await resp.Content.ReadAsStringAsync();

            if (!resp.IsSuccessStatusCode)
            {
                _logger.LogError($"Could not send email for template {templateViewName}: {resp.StatusCode}-{emailModel.ToEmail} {body}");
            }
            else
            {
                await SendEmailAsync(emailAddresses, emailModel.Subject, body);
            }
        }
    }
}
