﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mindgaze.AspNetCore.Email
{
    public class EmailModel
    {
        public string ToEmail { get; set; }

        public string Subject { get; set; }
    }
}
