﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mindgaze.AspNetCore.Email
{
    public class EmailTemplateConfiguration
    {
        public string TemplateServiceUrl { get; set; }

        public string TemplateController { get; set; }
    }
}
