﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Mindgaze.AspNetCore.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mindgaze.AspNetCore.Email
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddEmailServices(this IServiceCollection services, Action<EmailConfiguration> action)
        {
            var config = new EmailConfiguration();

            services.Configure(action);
            services.AddScoped<IEmailSender, EmailSender>();

            return services;
        }

        public static IServiceCollection AddEmailServices(this IServiceCollection services, IConfiguration configuration)
        {
            var config = new EmailConfiguration();

            services.Configure<EmailConfiguration>(configuration);
            services.AddScoped<IEmailSender, EmailSender>();

            return services;
        }

        public static IServiceCollection AddEmailTestingServices(this IServiceCollection services)
        {
            var emailSenderTester = new EmailSenderTester();

            services.AddSingleton<IEmailSender>(emailSenderTester);
            services.AddSingleton<IEmailSenderTester>(emailSenderTester);

            return services;
        }
    }
}
