﻿using Mindgaze.AspNetCore.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Email
{
    class EmailSenderTester : IEmailSender, IEmailSenderTester
    {
        private readonly List<Tuple<string, string, string>> _simpleEmails = new List<Tuple<string, string, string>>();
        private readonly List<Tuple<string, string, string>> _templateEmails = new List<Tuple<string, string, string>>();
        private readonly Dictionary<string, EmailModel> _templateEmailModels = new Dictionary<string, EmailModel>();

        public bool HasEmailWithDetails(string emailAddress, string subject, string messageBody)
        {
            return _simpleEmails.Contains(Tuple.Create(emailAddress, subject, messageBody));
        }

        public bool HasTemplatedEmail(string emailAddress, string subject, string templateViewName)
        {
            return _templateEmails.Contains(Tuple.Create(emailAddress, subject, templateViewName));
        }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            _simpleEmails.Add(new Tuple<string, string, string>(email, subject, message));

            return Task.CompletedTask;
        }

        public async Task SendEmailAsync(IReadOnlyCollection<string> emailAddresses, string subject, string message)
        {
            foreach (var email in emailAddresses)
            {
                await SendEmailAsync(email, subject, message);
            }
        }

        public Task SendTemplatedEmailAsync(EmailModel emailModel, string templateViewName)
        {
            _templateEmails.Add(new Tuple<string, string, string>(emailModel.ToEmail, emailModel.Subject, templateViewName));
            _templateEmailModels.Add($"{emailModel.ToEmail}{emailModel.Subject}{templateViewName}", emailModel);

            return Task.CompletedTask;
        }

        public async Task SendTemplatedEmailAsync(IReadOnlyCollection<EmailModel> emailModels, string templateViewName)
        {
            foreach (var model in emailModels)
            {
                await SendTemplatedEmailAsync(model, templateViewName);
            }
        }

        public Task SendTemplatedEmailAsync(IReadOnlyCollection<string> emailAddresses, EmailModel emailModel, string templateViewName)
        {
            foreach(var email in emailAddresses)
            {
                _templateEmails.Add(new Tuple<string, string, string>(email, emailModel.Subject, templateViewName));
                _templateEmailModels.Add($"{emailModel}{emailModel.Subject}{templateViewName}", emailModel);
            }

            return Task.CompletedTask;
        }

        public TModel GetEmailModel<TModel>(string email, string subject, string templateViewName)
            where TModel : EmailModel
        {
            var key = $"{email}{subject}{templateViewName}";

            if (!_templateEmailModels.ContainsKey(key))
            {
                throw new KeyNotFoundException("Could not find requested mail message!");
            }

            return _templateEmailModels[key] as TModel;
        }

        public Task<string> RenderTemplatedEmailAsync(EmailModel emailModel, string templateViewName)
        {
            return Task.FromResult(templateViewName);
        }

        public void ClearEmails()
        {
            _simpleEmails.Clear();
            _templateEmails.Clear();
            _templateEmailModels.Clear();
        }
    }
}
