﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mindgaze.AspNetCore.Email
{
    public class EmailConfiguration
    {
        public string Host { get; set; }

        public int Port { get; set; }

        public bool EnableSsl { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Sender { get; set; }

        public string HttpClientName { get; set; }

        public EmailTemplateConfiguration Template { get; set; }
    }
}
