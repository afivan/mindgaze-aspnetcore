using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Mindgaze.AspNetCore.Dto;
using Mindgaze.AspNetCore.DtoUtils;
using Mindgaze.AspNetCore.Extensions;

namespace Mindgaze.AspNetCore.Filters
{
    public class DependencyDtoActionFilter : IActionFilter
    {
        private readonly ILogger<DependencyDtoActionFilter> Logger;
        public DependencyDtoActionFilter(ILogger<DependencyDtoActionFilter> logger)
        {
            Logger = logger;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            // Supply to arguments
            //SupplyServices(context.HttpContext.RequestServices, context.ActionArguments.Select(x => x.Value));
            // TODO Array properties

            Func<object, IEnumerable<object>> traverseFunc = x => 
            {
                return x == null ? new object[]{} :
                    x.GetType().GetProperties().Where(p => p.PropertyType.IsAssignableToGenericType(typeof(ModelDto<>)) ).Select(p => p.GetValue(x))
                    .Union(
                        x.GetType().GetProperties().Where(p => p.PropertyType.GetTypeInfo().IsGenericType && p.PropertyType.GetTypeInfo().GetGenericTypeDefinition().IsAssignableToGenericType(typeof(IEnumerable<>)) && p.PropertyType.GetTypeInfo().GetGenericArguments().ElementAt(0).IsAssignableToGenericType(typeof(ModelDto<>)) ).SelectMany(p => p.GetValue(x) == null ? new object[]{} : ((IEnumerable<object>)p.GetValue(x)).ToArray()) 
                        )
                    ;
            };

            // Supply to properties
            var propertyValuesDeep = context.ActionArguments
                .Select(x => x.Value)
                //.Traverse(x => x == null ? new object[]{} : x.GetType().GetProperties().Where(p => p.PropertyType.IsAssignableToGenericType(typeof(ModelDto<>)) ).Select(p => p.GetValue(x)) )
                .Traverse(traverseFunc)
                .Where(x => x != null)
                // .Select(x => new { Obj = x, Props = x.GetType().GetProperties() })
                // .Select(x => new { Obj = x.Obj, PropValues = x.Props.Select(p => p.GetValue(x.Obj)) })
                // .Traverse(o => o.PropValues.Select(x => new { Obj = x, PropValues = x.Props.Select(p => p.GetValue(x.Obj)) }))
                ;

            Logger.LogDebug($"Found {propertyValuesDeep.Count()} values for potential DI");
            
            SupplyServices(context.HttpContext.RequestServices, propertyValuesDeep.ToArray());
        }

        private void SupplyServices(IServiceProvider serviceProvider, params object[] values)
        {
            var argsWithInterfaces = values
                .Where(v => v != null)
                .Select(x => new { Arg = x, Type = x.GetType() })
                .Select(x => new { 
                    Arg = x.Arg, 
                    Interfaces = x.Type.GetInterfaces()
                        .Where(i => i.GetTypeInfo().IsGenericType && (i.GetGenericTypeDefinition().Equals(typeof(IDependencyDtoServiceSupplier<>)) || i.GetGenericTypeDefinition().Equals(typeof(IDependencyDtoServiceSupplier<,>)) || i.GetGenericTypeDefinition().Equals(typeof(IDependencyDtoServiceSupplier<,,>)) || i.GetGenericTypeDefinition().Equals(typeof(IDependencyDtoServiceSupplier<,,,>))) )})
                .Where(x => x.Interfaces?.Count() > 0)
                ;

            Logger.LogDebug($"Supplying DI parameters for {argsWithInterfaces.Count()} properties");

            foreach(var argInterface in argsWithInterfaces)
            {
                foreach(var supplyInterface in argInterface.Interfaces)
                {
                    var genericArgumentTypes = supplyInterface.GetGenericArguments();

                    IDependencyDtoServiceSupplier<object> auxDtoSupp = null;
                    
                    var supplyMethodInfo = argInterface.Arg.GetType().GetMethod(nameof(auxDtoSupp.SupplyServices), genericArgumentTypes);

                    List<object> resolvedValues = new List<object>(genericArgumentTypes.Length);

                    foreach(var genericType in genericArgumentTypes)
                    {
                        resolvedValues.Add(serviceProvider.GetService(genericType));
                    }

                    supplyMethodInfo.Invoke(argInterface.Arg, resolvedValues.ToArray());
                }
            }
        }
    }
}