﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Mindgaze.AspNetCore.Extensions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mindgaze.AspNetCore.Attributes
{
    public sealed class SameSubnetAttribute : ActionFilterAttribute
    {
        public SameSubnetAttribute()
        {
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var httpContext = context.HttpContext;
            var logger = context.HttpContext.RequestServices.GetService<ILogger<SameSubnetAttribute>>() ?? NullLogger<SameSubnetAttribute>.Instance;

            var clientAddress = httpContext.Connection.RemoteIpAddress.MapToIPv4();
            var localAddress = httpContext.Connection.LocalIpAddress.MapToIPv4();
            var subnetMask = localAddress.GetSubnetMask();

            logger.LogInformation($"Same subnet verification remote: {clientAddress} local: {localAddress} subnet: {subnetMask}");

            if (subnetMask != null && !clientAddress.IsInSameSubnet(localAddress, subnetMask))
            {
                context.Result = new NotFoundResult();
            }
        }
    }
}
