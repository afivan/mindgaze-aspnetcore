﻿using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mindgaze.AspNetCore.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq.Expressions;
using Mindgaze.AspNetCore.ActionContexts;

namespace Mindgaze.AspNetCore.Controllers
{
    public abstract class BaseController : Controller
    {
        #region Private members

        #region COnstants


        #endregion

        #region Variables

        

        #endregion

        #region Properties

        #endregion

        #region Methods


        #endregion

        #endregion

        #region Protected members

        

        #region Readonly vars
        
        #endregion

        #region Abstract

        #endregion

        #region Properties

        protected PagingContext PagingContext
        {
            get;
            private set;
        }

        protected FilteringContext FilteringContext
        {
            get;
            private set;
        }

        protected SortingContext SortingContext
        {
            get;
            private set;
        }

        protected IncludeContext IncludingContext
        {
            get;
            private set;
        }

        #endregion

        #region Methods

        protected virtual void SupplyPagingContextOptions(out int maximumRecordsPerPage, out int defaultRecordsPerPage)
        {
            maximumRecordsPerPage = 100;
            defaultRecordsPerPage = maximumRecordsPerPage / 2;
        }

        #endregion

        #endregion

        #region Constructors

        protected BaseController()
        {
            
        }
        

        #endregion

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            int maximumRecordsPerPage, defaultRecordsPerPage;
            SupplyPagingContextOptions(out maximumRecordsPerPage, out defaultRecordsPerPage);

            PagingContext = new PagingContext(context.HttpContext, maximumRecordsPerPage, defaultRecordsPerPage);
            FilteringContext = new FilteringContext(context.HttpContext);
            SortingContext = new SortingContext(context.HttpContext);
            IncludingContext = new IncludeContext(context.HttpContext);
        }
    }
}
