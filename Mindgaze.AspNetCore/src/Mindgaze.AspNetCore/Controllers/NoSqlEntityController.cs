﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mindgaze.AspNetCore.Attributes;
using Mindgaze.AspNetCore.Binders;
using Mindgaze.AspNetCore.Dto;
using Mindgaze.AspNetCore.ModelEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Controllers
{
    public abstract class NoSqlEntityController<TEntity, TKey, TDbContext, TDto> : EntityController<TEntity, TKey, TDbContext, TDto>
        where TEntity : NoSqlEntity<TKey>
        where TKey : IEquatable<TKey>
        where TDbContext : DbContext
        where TDto : NoSqlDto<TKey>
    {
        private readonly IMapper _mapper;
        private readonly TDbContext _dbContext;

        private Type GetDtoTypeWithCheck(string requestedType)
        {
            var type = GetDtoType(requestedType);

            if (type != null && !typeof(NoSqlDto<TKey>).IsAssignableFrom(type))
            {
               throw new InvalidOperationException("Supplied type is not compatible with NoSql type!");
            }

            return type;
        }
        private async Task<IActionResult> ValidateModelAndExecute(Func<object, Task<IActionResult>> executeAsync)
        {
            var noSqlDtoJson = await JsonDocument.ParseAsync(Request.Body);

            if (noSqlDtoJson.RootElement.TryGetProperty("type", out JsonElement jsonTypeElement))
            {
                var requestedType = jsonTypeElement.GetString();
                var type = GetDtoTypeWithCheck(requestedType);

                if (type != null)
                {
                    var dtoObject = JsonSerializer.Deserialize(noSqlDtoJson.RootElement.ToString(), type, new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });

                    DiModelBinder.SupplyInterfaces(dtoObject, HttpContext.RequestServices);
                    DiModelBinder.SupplyToProperties(dtoObject, HttpContext.RequestServices);

                    if (TryValidateModel(dtoObject))
                    {
                        return await executeAsync(dtoObject);
                    }
                    else
                    {
                        return BadRequest(new ErrorDto(ModelState));
                    }
                }

                return BadRequest(new ErrorDto() { Error = $"Unimplemented type {requestedType}!" });
            }

            return BadRequest(new ErrorDto() { Error = "Could not get type from request!" });
        }

        public NoSqlEntityController(TDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        protected abstract Type GetDtoType(string type);

        protected Func<TDto, TDto> GetDeriveFunc()
        {
            return dto => 
            {
                var type = GetDtoTypeWithCheck(dto.Type);

                if (type == null)
                {
                    return dto;
                }

                var entity = _mapper.Map<TEntity>(dto);
                return (TDto)_mapper.Map(entity, entity.GetType(), type);
            };
        }

        protected async Task<IActionResult> GetNoSqlAsync(IQueryable<TEntity> prefiltered)
        {
            return await GetAsync(prefiltered, GetDeriveFunc());
        }

        [NonAction]
        public override Task<IActionResult> PatchAsync([FromBody] JsonPatchRequestDto<TDto, TKey> patchRequest)
        {
            throw new NotImplementedException();
        }

        [NonAction]
        public override Task<IActionResult> PostAsync([FromBody] TDto dto)
        {
            throw new NotImplementedException();
        }

        [NonAction]
        public override Task<IActionResult> PutAsync([FromBody] TDto dto)
        {
            throw new NotImplementedException();
        }

        [NonAction]
        public override Task<IActionResult> DeleteAsync([FromBody] TDto value)
        {
            throw new NotImplementedException();
        }

        [NonAction]
        public override Task<IActionResult> DeleteAsync(TKey id)
        {
            throw new NotImplementedException();
        }

        [NonAction]
        public override Task<IActionResult> GetAsync()
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(typeof(ErrorDto), 400)]
        public virtual async Task<IActionResult> PostNoSqlEntityAsync()
        {
            return await ValidateModelAndExecute(async dto => await CreateInternalAsync(dto));
        }

        [HttpPut]
        [ProducesResponseType(201)]
        [ProducesResponseType(typeof(ErrorDto), 400)]
        public virtual async Task<IActionResult> PutNoSqlEntityAsync()
        {
            return await ValidateModelAndExecute(async dto => await UpdateInternalAsync(dto));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorDto), 400)]
        [ProducesResponseType(404)]
        public virtual async Task<IActionResult> DeleteNoSqlEntityAsync(TKey id)
        {
            var type = (await EntityDbSetPropertyFunc(_dbContext).FindAsync(id))?.Type;

            var dtoType = GetDtoTypeWithCheck(type);

            if (dtoType != null)
            {
                return await DeleteInternalAsync(id, dtoType);
            }

            return NotFound();
        }

        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(ErrorDto), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public virtual async Task<IActionResult> DeleteNoSqlEntityAsync()
        {
            return await ValidateModelAndExecute(async dto =>
            {
                var entity = _mapper.Map<TEntity>(dto);
                return await DeleteInternalAsync(entity.Id, dto.GetType());
            });
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginationDto<>), 200)]
        public virtual async Task<IActionResult> GetNoSqlEntityAsync()
        {
            return await GetNoSqlAsync(EntityDbSetPropertyFunc(_dbContext));
        }
    }
}
