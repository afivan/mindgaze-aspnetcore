using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mindgaze.AspNetCore.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq.Expressions;
using Mindgaze.AspNetCore.ActionContexts;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using Mindgaze.AspNetCore.Helpers;
using Microsoft.Extensions.Primitives;
using System.Text;
using Mindgaze.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Mindgaze.Utilities.Helpers;
using Swashbuckle.AspNetCore.Annotations;
using Mindgaze.AspNetCore.ModelEntities;
using Microsoft.AspNetCore.JsonPatch.Exceptions;

namespace Mindgaze.AspNetCore.Controllers
{
    [SwaggerSchemaFilter(typeof(ActionContextsOperationFilter))]
    public abstract class EntityController<TEntity, TKey, TDbContext, TDto> : BaseController
        where TEntity: class
        where TKey: IEquatable<TKey>
        where TDbContext: DbContext
        where TDto : class
    {
        #region Private members

        #region COnstants

        private const int MaximumRecordsPerPage = 100;
        private const int DefaultRecordsPerPage = MaximumRecordsPerPage / 2;

        #endregion

        #region Variables

        private readonly TDbContext DbContext;
        private readonly IMapper Mapper;

        #region From expressions

        private readonly DbSet<TEntity> EntityDbSet;

        #endregion

        #endregion

        #region Properties

        //private PropertyInfo KeyPropertyInfo
        //{
        //    get;set;
        //}

        #endregion

        #region Methods

        private async Task<IActionResult> EncapsulateToActionResult(IQueryable<TDto> projectedQuery, IEnumerable<object> contractedQuery)
        {
            var projectedQueryCount = await projectedQuery.CountAsync();
            var items = contractedQuery.AsEnumerable().ToArray();
            var contractedQueryCount = items.Length;

            var paginationDto = new PaginationDto<object>()
            {
                Items = items,
                Pager = new PagerDto()
                {
                    CurrentPageNumber = PagingContext.Page,
                    RecordsPerPage = PagingContext.RecordsPerPage,
                    CurrentPageRecords = contractedQueryCount,
                    TotalPages = projectedQueryCount / PagingContext.RecordsPerPage,
                    TotalRecords = projectedQueryCount,
                }
            };

            if (projectedQueryCount % PagingContext.RecordsPerPage != 0)
            {
                paginationDto.Pager.TotalPages++;
            }

            // TODO Next & prev urls
            StringBuilder urlBackBone = new StringBuilder($"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.Value}{HttpContext.Request.Path}");

            KeyValuePair<string, StringValues> exceptForPageParam = new KeyValuePair<string, StringValues>(PagingContext.PageParam, HttpContext.Request.Query[PagingContext.PageParam]);
            KeyValuePair<string, StringValues> exceptForRecordsPerPageParam = new KeyValuePair<string, StringValues>(PagingContext.RecordsPerPageParam, HttpContext.Request.Query[PagingContext.RecordsPerPageParam]);

            var query = HttpContext.Request.Query
                .Except(new[] { exceptForPageParam, exceptForRecordsPerPageParam })
                .Select(q => $"{q.Key}={q.Value}")
                ;

            if (paginationDto.Pager.CurrentPageNumber > 1)
            {
                // Build PrevPageUri
                var prevOriginalParams = query.ToList();
                prevOriginalParams.Add($"{PagingContext.PageParam}={paginationDto.Pager.CurrentPageNumber - 1}");
                prevOriginalParams.Add($"{PagingContext.RecordsPerPageParam}={paginationDto.Pager.RecordsPerPage}");

                paginationDto.Pager.PrevPageUri = $"{urlBackBone}?{prevOriginalParams.Aggregate((qPairLeft, qPairRight) => $"{qPairLeft}&{qPairRight}")}";
            }

            if (paginationDto.Pager.CurrentPageNumber < paginationDto.Pager.TotalPages)
            {
                // Build NextPageUri
                var nextOriginalParams = query.ToList();
                nextOriginalParams.Add($"{PagingContext.PageParam}={paginationDto.Pager.CurrentPageNumber + 1}");
                nextOriginalParams.Add($"{PagingContext.RecordsPerPageParam}={paginationDto.Pager.RecordsPerPage}");

                paginationDto.Pager.NextPageUri = $"{urlBackBone}?{nextOriginalParams.Aggregate((qPairLeft, qPairRight) => $"{qPairLeft}&{qPairRight}")}";
            }

            if (paginationDto.Pager.TotalPages != 1 && paginationDto.Pager.CurrentPageNumber > 1)
            {
                // Build FirstPageUri
                var prevOriginalParams = query.ToList();
                prevOriginalParams.Add($"{PagingContext.PageParam}=1");
                prevOriginalParams.Add($"{PagingContext.RecordsPerPageParam}={paginationDto.Pager.RecordsPerPage}");

                paginationDto.Pager.FirstPageUri = $"{urlBackBone}?{prevOriginalParams.Aggregate((qPairLeft, qPairRight) => $"{qPairLeft}&{qPairRight}")}";
            }

            if (paginationDto.Pager.TotalPages != 1 && paginationDto.Pager.CurrentPageNumber < paginationDto.Pager.TotalPages)
            {
                // Build LastPageUri
                var nextOriginalParams = query.ToList();
                nextOriginalParams.Add($"{PagingContext.PageParam}={paginationDto.Pager.TotalPages}");
                nextOriginalParams.Add($"{PagingContext.RecordsPerPageParam}={paginationDto.Pager.RecordsPerPage}");

                paginationDto.Pager.LastPageUri = $"{urlBackBone}?{nextOriginalParams.Aggregate((qPairLeft, qPairRight) => $"{qPairLeft}&{qPairRight}")}";
            }

            //return Json(paginationDto, new System.Text.Json.JsonSerializerOptions
            //{
                
            //});

            return Ok(paginationDto);
        }

        private Expression<Func<TEntity, bool>> GenerateKeyEqualityExpression(IEnumerable<TKey> keys)
        {
            var entityType = typeof(TEntity);
            var entityTypeInfo = entityType.GetTypeInfo();

            var argumentExp = Expression.Parameter(entityType);
            Expression orBodyExpression = null;

            foreach(var key in keys)
            {
                var eqExp = GenerateKeyEqualityExpression(key);
                //System.Linq.Expressions.LogicalBinaryExpression
                //var logicalBody = (LogicalBinaryExpression)eqExp.Body;
                BinaryExpression bodyExp = (BinaryExpression)eqExp.Body;
                MemberExpression leftParamExp = (MemberExpression)bodyExp.Left;
                leftParamExp = leftParamExp.Update(argumentExp);
                bodyExp = bodyExp.Update(leftParamExp, bodyExp.Conversion, bodyExp.Right);

                //eqExp = eqExp.Update(bodyExp, new ParameterExpression[] { argumentExp });
                if(orBodyExpression == null)
                {
                    orBodyExpression = bodyExp;
                }
                else
                {
                    orBodyExpression = Expression.Or(orBodyExpression, bodyExp);
                }
            }

            return Expression.Lambda<Func<TEntity, bool>>(orBodyExpression, argumentExp);
        }

        #region Key properties

        #endregion

        #endregion

        #endregion

        #region Protected members



        #region Readonly vars


        #endregion

        #region Abstract

        protected abstract Func<TDbContext, DbSet<TEntity>> EntityDbSetPropertyFunc
        {
            get;
        }

        protected abstract Func<TEntity, TKey> EntityGetKeyFunc
        {
            get;
        }
        
        protected abstract Action<TEntity, TKey> EntitySetKeyAction
        {
            get;
        }
        

        protected abstract Expression<Func<TEntity, bool>> GenerateKeyEqualityExpression(TKey key);

        #endregion

        #region Properties


        #endregion

        #region Methods

        protected async Task<bool> EntityExistsAsync(TEntity entity)
        {
            return await EntityExistsAsync(EntityGetKeyFunc(entity));
        }

        protected async Task<bool> EntityExistsAsync(TKey key)
        {
            var projectedQuery = EntityDbSet
                .AsNoTracking()
                .Where(GenerateKeyEqualityExpression(key))
                .ProjectTo<TDto>(Mapper.ConfigurationProvider)
                ;

            if (CustomFilter != null)
            {
                projectedQuery = projectedQuery.Where(CustomFilter);
            }

            return await projectedQuery.AnyAsync();
        }

        protected delegate TDerivedDto DerivedDtoByTypeDelegate<TDerivedDto>(string type) where TDerivedDto : TDto;

        protected async Task<IActionResult> GetAsync(IQueryable<TEntity> customFiltered, Func<TDto, TDto> deriveFunc = null)
        {
            var page = PagingContext.Page;
            var recordsPerPage = PagingContext.RecordsPerPage;

            // TODO where clauses for derived not working
            var wherePredicate = FilteringContext.GetWhereClausePredicate<TDto>();
            var orderByAscending = SortingContext.GetOrderByAscending<TEntity>();
            var orderByDescending = SortingContext.GetOrderByDescending<TEntity>();

            if (orderByAscending != null)
            {
                customFiltered = customFiltered.OrderBy(orderByAscending);
            }

            if (orderByDescending != null)
            {
                customFiltered = customFiltered.OrderByDescending(orderByDescending);
            }

            if (orderByAscending != null || orderByDescending != null)
            {
                var orderedQuery = (IOrderedQueryable<TEntity>)customFiltered;

                foreach (var thenByAsc in SortingContext.GetThenByAscending<TEntity>())
                {
                    orderedQuery = orderedQuery.ThenBy(thenByAsc);
                }

                foreach (var thenByDesc in SortingContext.GetThenByDescending<TEntity>())
                {
                    orderedQuery = orderedQuery.ThenByDescending(thenByDesc);
                }

                customFiltered = orderedQuery;
            }

            var projectedQuery = customFiltered
                .AsNoTracking()
                .ProjectTo<TDto>(Mapper.ConfigurationProvider, IncludingContext
                    .GetIncludedPropertyValues<TDto>()
                    .Where(x => x != null)
                    .ToArray())
            ;

            var combinedWhere = ExpressionUtils.MultipleAndAlso(CustomFilter, wherePredicate);

            if (combinedWhere != null)
            {
                projectedQuery = projectedQuery.Where(combinedWhere);
            }

            var contractedQuery = projectedQuery
                    .Skip((page - 1) * recordsPerPage)
                    .Take(recordsPerPage)
                    .AsEnumerable()
                    ;

            // TODO Order thenBy

            if (deriveFunc == null)
            {
                return await EncapsulateToActionResult(projectedQuery, contractedQuery);
            }
            else
            {
                var derivedDtos = new List<TDto>();

                foreach (var baseDto in contractedQuery)
                {
                    derivedDtos.Add(deriveFunc(baseDto));
                }

                return await EncapsulateToActionResult(projectedQuery, derivedDtos);
            }
        }

        protected virtual Expression<Func<TDto, bool>> CustomFilter
        {
            get
            {
                return null;
            }
        }

        protected virtual IEnumerable<Expression<Func<TDto, object>>> PatchIncludeProperties
        {
            get
            {
                return new Expression<Func<TDto, object>>[0];
            }
        }

        #endregion

        #region Internal

        internal async Task<IActionResult> CreateInternalAsync(object dto)
        {
            var entity = Mapper.Map<TEntity>(dto);
            // Ensure key has a default value
            EntitySetKeyAction(entity, default);

            var entry = await EntityDbSet.AddAsync(entity);

            if (entry.State == EntityState.Added)
            {
                await DbContext.SaveChangesAsync();

                return Created(HttpContext.Request.Path, Mapper.Map(entry.Entity, typeof(TEntity), dto.GetType()));
            }

            return BadRequest(new ErrorDto
            {
                Error = "Unexpected error while adding entity!",
            });
        }

        internal async Task<IActionResult> UpdateInternalAsync(object dto)
        {
            var entity = Mapper.Map<TEntity>(dto);

            if (!await EntityExistsAsync(entity))
            {
                return NotFound();
            }

            var state = EntityDbSet.Update(entity);

            if (state.State == EntityState.Modified)
            {
                await DbContext.SaveChangesAsync();

                return Ok(Mapper.Map(entity, typeof(TEntity), dto.GetType()));
            }

            return NotFound();
        }

        internal async Task<IActionResult> DeleteInternalAsync(TKey id, Type dtoType)
        {
            var entity = await EntityDbSet.FindAsync(id);

            if (entity != null)
            {
                if (CustomFilter != null && !CustomFilter.Compile()((TDto)Mapper.Map(entity, typeof(TEntity), dtoType)))
                {
                    return NotFound();
                }

                EntityDbSet.Remove(entity);

                await DbContext.SaveChangesAsync();

                return NoContent();
            }

            return NotFound();
        }

        #endregion

        #endregion

        #region Constructors

        public EntityController(TDbContext dbContext, IMapper mapper)
        {
            DbContext = dbContext;
            Mapper = mapper;

            EntityDbSet = EntityDbSetPropertyFunc(DbContext);
        }
        

        #endregion

        // Need POST, PUT PATCH HEAD

        // Categorize controllers as
        /*
         Operation COntroller - like auth
         Entity controller - dealing with entities, filtering searching bla bla
         File/Binary controller - BLOBS
        */

        /*
         * Include for nested complex properties with their pagination
        */
        
        // GET: [controller]
        [HttpGet]
        [ProducesResponseType(typeof(PaginationDto<>), 200)]
        public virtual async Task<IActionResult> GetAsync()
        {
            return await GetAsync(EntityDbSet);
        }
        
        // POST api/[controller]
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(typeof(ErrorDto), 400)]
        public async virtual Task<IActionResult> PostAsync([FromBody]TDto dto)
        {
            if (ModelState.IsValid && dto != null)
            {
                return await CreateInternalAsync(dto);
            }

            return BadRequest(new ErrorDto(ModelState));
        }

        // PUT api/[controller]/5
        [HttpPut]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorDto), 400)]
        [ProducesResponseType(404)]
        public virtual async Task<IActionResult> PutAsync([FromBody]TDto dto)
        {
            if (ModelState.IsValid && dto != null)
            {
                return await UpdateInternalAsync(dto);
            }

            return BadRequest(new ErrorDto(ModelState));
        }

        // DELETE api/[controller]/5
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorDto), 400)]
        [ProducesResponseType(404)]
        public async virtual Task<IActionResult> DeleteAsync(TKey id)
        {
            return await DeleteInternalAsync(id, typeof(TDto));
        }

        // DELETE api/[controller]
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(ErrorDto), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async virtual Task<IActionResult> DeleteAsync([FromBody]TDto value)
        {
            if (value != null)
            {
                var entity = Mapper.Map<TEntity>(value);
                return await DeleteAsync(EntityGetKeyFunc(entity));
            }

            return BadRequest(new ErrorDto(ModelState));
        }

        [HttpHead("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async virtual Task<IActionResult> HeadAsync(TKey id)
        {
            if (!await EntityExistsAsync(id))
            {
                return NotFound();
            }

            return Ok();
        }

        [HttpPatch]
        // TODO List of ids instead 
        public async virtual Task<IActionResult> PatchAsync([FromBody] JsonPatchRequestDto<TDto, TKey> patchRequest)
        {
            if (ModelState.IsValid && patchRequest != null)
            {
                if(patchRequest.Operations == null || patchRequest.Operations.Count<=0)
                {
                    return BadRequest(new ErrorDto { Error = "No operation specified!" });
                }

                var keyNameJson = EntityKeyPropertyStore.LowercaseFirst(EntityKeyPropertyStore.GetKeyPropertyName<TEntity, TKey>());
                
                foreach(var op in patchRequest.Operations)
                {
                    if(op.path?.Replace("/", "") == keyNameJson)
                    {
                        return BadRequest(new ErrorDto { Error = "Entity key cannot be touched!" });
                    }
                }

                // Block any attempts to change read only props
                var blockedAttempts = patchRequest.Operations.RemoveAll(o => EntityKeyPropertyStore.GetReadOnlyProperties<TDto>().Select(x => EntityKeyPropertyStore.LowercaseFirst(x)).Contains(o.path?.Replace("/", "")) );

                if (blockedAttempts > 0)
                {
                    return BadRequest(new ErrorDto { Error = $"Detected {blockedAttempts} to change readonly props!" });
                }

                var projectedQueryable = EntityDbSet.Where(GenerateKeyEqualityExpression(patchRequest.Ids))
                    .ProjectTo(Mapper.ConfigurationProvider, PatchIncludeProperties.ToArray());

                if (CustomFilter != null)
                {
                    projectedQueryable = projectedQueryable.Where(CustomFilter);
                }

                var broughtEntities = await projectedQueryable.ToListAsync();

                if (broughtEntities.Count != patchRequest.Ids.Count())
                {
                    return BadRequest(new ErrorDto { Error = $"Could not find all the requested entities. Requested: {patchRequest.Ids.Count()} Found: {broughtEntities.Count}" });
                }

                List<TEntity> updatedEntities = new List<TEntity>();
                List<ErrorDto> validationErrors = new List<ErrorDto>();

                foreach (var dto in broughtEntities)
                {
                    var patch = patchRequest.GetJsonPatchDoc();
                    try
                    {
                        patch.ApplyTo(dto);
                    }
                    catch(JsonPatchException jEx)
                    {
                        return BadRequest(new ErrorDto { Error = jEx.Message });
                    }
                    // Perform DTO valiation
                    var entity = Mapper.Map<TEntity>(dto);

                    if (!TryValidateModel(dto))
                    {
                        validationErrors.Add(new ErrorDto(ModelState) { EntityId = EntityGetKeyFunc(entity) });
                    }

                    updatedEntities.Add(entity);
                }

                if (!validationErrors.Any())
                {
                    EntityDbSet.UpdateRange(updatedEntities);

                    var affectedRows = await DbContext.SaveChangesAsync();

                    return Ok(new JsonPatchResultDto { AffectedRows = affectedRows });
                }
                else
                {
                    return BadRequest(validationErrors);
                }
            }

            return BadRequest(new ErrorDto(ModelState));
        }
    }
}
