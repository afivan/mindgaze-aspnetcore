﻿using Mindgaze.AspNetCore.Email;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mindgaze.AspNetCore.Services
{
    public interface IEmailSenderTester
    {
        bool HasEmailWithDetails(string emailAddress, string subject, string messageBody);

        bool HasTemplatedEmail(string emailAddress, string subject, string templateViewName);

        TModel GetEmailModel<TModel>(string email, string subject, string templateViewName)
            where TModel : EmailModel;

        void ClearEmails();
    }
}
