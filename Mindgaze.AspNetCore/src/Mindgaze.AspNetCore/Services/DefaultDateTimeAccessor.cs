﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mindgaze.AspNetCore.Services
{
    public class DefaultDateTimeAccessor : IDateTimeAccessor
    {
        public DateTime GetCurrentDate()
        {
            return DateTime.Now.Date;
        }

        public DateTime GetCurrentDateTime()
        {
            return DateTime.Now;
        }

        public DateTime GetCurrentDateTimeUtc()
        {
            return DateTime.UtcNow;
        }

        public DateTime GetCurrentDateUtc()
        {
            return DateTime.UtcNow.Date;
        }

        public DateTime GetCurrentDateForTimezone(string timeZoneId)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(GetCurrentDateTimeUtc(), TimeZoneInfo.FindSystemTimeZoneById(timeZoneId));
        }

        public IEnumerable<string> TimeZoneIds => TimeZoneInfo.GetSystemTimeZones().Select(_ => _.Id);
    }
}
