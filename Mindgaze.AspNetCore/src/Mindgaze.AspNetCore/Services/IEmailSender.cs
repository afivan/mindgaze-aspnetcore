﻿using Mindgaze.AspNetCore.Email;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);

        Task SendEmailAsync(IReadOnlyCollection<string> emailAddresses, string subject, string message);

        Task SendTemplatedEmailAsync(EmailModel emailModel, string templateViewName);

        Task SendTemplatedEmailAsync(IReadOnlyCollection<EmailModel> emailModels, string templateViewName);

        Task SendTemplatedEmailAsync(IReadOnlyCollection<string> emailAddresses, EmailModel emailModel, string templateViewName);

        Task<string> RenderTemplatedEmailAsync(EmailModel emailModel, string templateViewName);
    }
}
