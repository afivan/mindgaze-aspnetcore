﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mindgaze.AspNetCore.Services
{
    public interface IDateTimeAccessor
    {
        DateTime GetCurrentDateTime();
        DateTime GetCurrentDateTimeUtc();

        DateTime GetCurrentDate();
        DateTime GetCurrentDateUtc();

        DateTime GetCurrentDateForTimezone(string timeZoneId);
        IEnumerable<string> TimeZoneIds { get; }
    }
}
