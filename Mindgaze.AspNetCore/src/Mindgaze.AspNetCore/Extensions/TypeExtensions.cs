using Mindgaze.AspNetCore.DtoUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Mindgaze.AspNetCore.Extensions
{
    public static class TypeExtensions
    {
        public static bool IsAssignableToGenericType(this Type givenType, Type genericType)
        {
            var interfaceTypes = givenType.GetInterfaces();

            foreach (var it in interfaceTypes)
            {
                if (it.GetTypeInfo().IsGenericType && it.GetGenericTypeDefinition() == genericType)
                    return true;
            }

            if (givenType.GetTypeInfo().IsGenericType && givenType.GetGenericTypeDefinition() == genericType)
                return true;

            Type baseType = givenType.GetTypeInfo().BaseType;
            if (baseType == null) return false;

            return IsAssignableToGenericType(baseType, genericType);
        }

        public static bool HasConstructorsWithParameters(this Type type)
        {
            return type.GetConstructors().Where(c => c.GetParameters().Length > 0).Any();
        }

        public static bool IsDependencyDtoServiceSupplier(this Type type)
        {
            return type.GetTypeInfo().IsGenericType && (
                type.GetGenericTypeDefinition().Equals(typeof(IDependencyDtoServiceSupplier<>))
                ||
                type.GetGenericTypeDefinition().Equals(typeof(IDependencyDtoServiceSupplier<,>))
                ||
                type.GetGenericTypeDefinition().Equals(typeof(IDependencyDtoServiceSupplier<,,>))
                ||
                type.GetGenericTypeDefinition().Equals(typeof(IDependencyDtoServiceSupplier<,,,>))
                )
            ;
        }

        public static bool HasDependencyDtoServiceSupplierImplemented(this Type type)
        {
            return type.GetInterfaces().Where(i =>
                i.IsDependencyDtoServiceSupplier())
                .Any();
        }

        public static bool HasDependencyDtoServiceSupplierImplementedByCollection(this Type type)
        {
            if (type.IsAssignableToGenericType(typeof(IEnumerable<>)) && type.GetTypeInfo().GetGenericTypeDefinition().HasDependencyDtoServiceSupplierImplemented())
            {
                return true;
            }

            return false;
        }

        public static bool HasDependencyDtoServiceSupplierImplementedByAnyProperty(this Type type)
        {
            Func<PropertyInfo, IEnumerable<PropertyInfo>> traverseFunc = x =>
            {
                return x == null ? new PropertyInfo[] { } :
                    x.ReflectedType.GetProperties()
                        .Where(p => p.PropertyType.HasDependencyDtoServiceSupplierImplemented())
                        .Union(x.GetType().GetProperties()
                            .Where(p => p.PropertyType.HasDependencyDtoServiceSupplierImplementedByCollection())
                            )
                        ;
            };

            return type.GetProperties().Traverse(traverseFunc).Any();
        }

        public static IEnumerable<object> GetSubObjectsWithDependencyDtoServiceSupplierImplemented(this Type type, object value)
        {
            Func<object, IEnumerable<object>> traverseFunc = x =>
            {
                return x == null ? new object[] { } :
                    x.GetType().GetProperties()
                        .Where(p => p.PropertyType.HasDependencyDtoServiceSupplierImplemented())
                        .Select(p => p.GetValue(x))
                        .Union(x.GetType().GetProperties()
                            .Where(p => p.PropertyType.HasDependencyDtoServiceSupplierImplementedByCollection())
                            .SelectMany(p => p.GetValue(x) == null ? new object[] { } : ((IEnumerable<object>)p.GetValue(x)).ToArray())
                            )
                        ;
            };

            return traverseFunc(value);
        }
    }
}