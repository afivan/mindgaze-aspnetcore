using System;
using System.Linq;
using System.Security.Claims;

namespace Mindgaze.AspNetCore.Utils
{
    public static class ClaimsPrincipalExtensions
    {
        public static string GetUserId(this ClaimsPrincipal user)
        {
            if(user == null)
            {
                return null;
            }

            return user.Claims.Single(c => c.Type == "sub").Value;
        }

        public static Guid GetUserGuid(this ClaimsPrincipal user)
        {
            return GetUserId(user) != null ? new Guid(GetUserId(user)) : default(Guid);
        }
    }
}