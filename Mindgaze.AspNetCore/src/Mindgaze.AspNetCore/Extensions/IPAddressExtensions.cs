﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace Mindgaze.AspNetCore.Extensions
{
    public static class IPAddressExtensions
    {
        public static IPAddress GetBroadcastAddress(this IPAddress address, IPAddress subnetMask)
        {
            byte[] ipAdressBytes = address.GetAddressBytes();
            byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

            if (ipAdressBytes.Length != subnetMaskBytes.Length)
                throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

            byte[] broadcastAddress = new byte[ipAdressBytes.Length];
            for (int i = 0; i < broadcastAddress.Length; i++)
            {
                broadcastAddress[i] = (byte)(ipAdressBytes[i] | (subnetMaskBytes[i] ^ 255));
            }
            return new IPAddress(broadcastAddress);
        }

        public static IPAddress GetNetworkAddress(this IPAddress address, IPAddress subnetMask)
        {
            byte[] ipAdressBytes = address.GetAddressBytes();
            byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

            if (ipAdressBytes.Length != subnetMaskBytes.Length)
                throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

            byte[] broadcastAddress = new byte[ipAdressBytes.Length];
            for (int i = 0; i < broadcastAddress.Length; i++)
            {
                broadcastAddress[i] = (byte)(ipAdressBytes[i] & (subnetMaskBytes[i]));
            }
            return new IPAddress(broadcastAddress);
        }

        public static bool IsInSameSubnet(this IPAddress address1, IPAddress address2, IPAddress subnetMask)
        {
            if (Object.Equals(address1, address2))
            {
                return true;
            }

            if (address1 == null)
            {
                throw new ArgumentNullException(nameof(address1));
            }

            if (address2 == null)
            {
                throw new ArgumentNullException(nameof(address2));
            }

            if (subnetMask == null)
            {
                throw new ArgumentNullException(nameof(subnetMask));
            }

            IPAddress network1 = address1.GetNetworkAddress(subnetMask);
            IPAddress network2 = address2.GetNetworkAddress(subnetMask);

            return network1.Equals(network2);
        }

        public static IPAddress GetSubnetMask(this IPAddress address)
        {
            if (address == null)
            {
                throw new ArgumentNullException(nameof(address));
            }

            foreach (var add in NetworkInterface.GetAllNetworkInterfaces())
            {
                var properties = add.GetIPProperties();

                var search = properties.UnicastAddresses.FirstOrDefault(a => a.Address.Equals(address));

                if (search != null)
                {
                    return search.IPv4Mask;
                }
            }

            return null;
        }
    }
}
