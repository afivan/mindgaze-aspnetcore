﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mindgaze.AspNetCore.Extensions
{
    public static class ActionResultExtensions
    {
        public static bool IsOkStatusResult(this IActionResult result)
        {
            return result is IStatusCodeActionResult statusCodeResult && statusCodeResult.StatusCode >= 200 && statusCodeResult.StatusCode < 300;
        }
    }
}
