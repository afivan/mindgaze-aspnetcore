﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Dto
{
    public class JsonPatchResultDto
    {
        public int AffectedRows
        {
            get;set;
        }
    }
}
