﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Dto
{
    public class PagerDto
    {
        public int CurrentPageNumber
        {
            get;set;
        }

        public int CurrentPageRecords
        {
            get;set;
        }

        public int TotalPages
        {
            get; set;
        }

        public int TotalRecords
        {
            get; set;
        }

        public int RecordsPerPage
        {
            get; set;
        }

        public string NextPageUri
        {
            get;set;
        }

        public string PrevPageUri
        {
            get; set;
        }

        public string FirstPageUri
        {
            get; set;
        }

        public string LastPageUri
        {
            get; set;
        }
    }
}
