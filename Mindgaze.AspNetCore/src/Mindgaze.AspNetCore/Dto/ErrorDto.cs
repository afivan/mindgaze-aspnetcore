﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Dto
{
    public class ErrorDto
    {
        public ErrorDto()
        {

        }

        public ErrorDto(ModelStateDictionary validationErrors)
        {
            ValidationErrors = new List<ValidationErrorDto>();

            foreach (var key in validationErrors.Keys)
            {
                ValidationErrors.Add(new ValidationErrorDto()
                {
                    Field = key,
                    Messages = validationErrors[key].Errors.Select(x => x.ErrorMessage).ToArray(),
                });
            
            }
        }

        public string Error
        {
            get;set;
        }

        public List<ValidationErrorDto> ValidationErrors
        {
            get; private set;
        }

        public object EntityId
        {
            get;set;
        }
    }

    public class ErrorDto<T>: ErrorDto
    {
        public T InternalErrors
        {
            get; set;
        }
    }

    public class ValidationErrorDto
    {
        [Required]
        public string Field
        {
            get;set;
        }

        [Required]
        public string[] Messages
        {
            get;set;
        }
    }
}
