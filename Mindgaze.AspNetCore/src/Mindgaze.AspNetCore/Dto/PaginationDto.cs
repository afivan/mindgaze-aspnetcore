﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Dto
{
    public class PaginationDto<TDto> where TDto: class
    {
        public PagerDto Pager
        {
            get;set;
        }

        public IEnumerable<TDto> Items
        {
            get;set;
        }
    }
}
