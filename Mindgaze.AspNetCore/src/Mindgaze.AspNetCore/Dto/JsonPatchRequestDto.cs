﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Dto
{
    public sealed class JsonPatchRequestDto<TModel, TKey>
        where TModel : class
        where TKey : IEquatable<TKey>
    {
        [Required]
        public List<Operation<TModel>> Operations { get; set; }

        [Required]
        public TKey[] Ids
        {
            get;set;
        }

        public JsonPatchDocument<TModel> GetJsonPatchDoc()
        {
            // TODO Check operations for attempts of readonly modifiers

            return new JsonPatchDocument<TModel>(Operations, new DefaultContractResolver());
        }
    }
}
