using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Mindgaze.AspNetCore.DtoUtils;
using Mindgaze.AspNetCore.Utils;
using Newtonsoft.Json;

namespace Mindgaze.AspNetCore.Dto
{
    public class UserAwareDto<TKey> : ModelDto<TKey>, IDependencyDtoServiceSupplier<IHttpContextAccessor>
        where TKey: IEquatable<TKey>
    {
        public virtual Guid UserId
        {
            get; private set;
        }

        public void SupplyServices(IHttpContextAccessor httpContextAccessor)
        {
            UserId = httpContextAccessor.HttpContext.User.GetUserGuid();
        }
    }
}