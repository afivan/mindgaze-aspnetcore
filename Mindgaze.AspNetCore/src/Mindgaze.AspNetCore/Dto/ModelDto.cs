using System;

namespace Mindgaze.AspNetCore.Dto
{
    public class ModelDto<TKey>
        where TKey: IEquatable<TKey>
    {
        public virtual TKey Id
        {
            get;set;
        }
    }
}