﻿using Microsoft.AspNetCore.Http;
using Mindgaze.AspNetCore.DtoUtils;
using Mindgaze.AspNetCore.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Mindgaze.AspNetCore.Dto
{
    public class NoSqlDto<TKey> : ModelDto<TKey>
        where TKey : IEquatable<TKey>
    {
        // Automapper projection error when this is abstract
        [ReadOnly(true)]
        public string Type { get; protected set; }

        [JsonIgnore]
        [ReadOnly(true)]
        public JsonDocument SpecificData { get; set; }
    }

    public class UserAwareNoSqlDto<TKey> : NoSqlDto<TKey>, IDependencyDtoServiceSupplier<IHttpContextAccessor>
        where TKey : IEquatable<TKey>
    {
        public virtual Guid UserId
        {
            get; private set;
        }

        public void SupplyServices(IHttpContextAccessor httpContextAccessor)
        {
            UserId = httpContextAccessor.HttpContext.User.GetUserGuid();
        }
    }
}
