﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace Mindgaze.AspNetCore.ModelEntities
{
    public class NoSqlEntity<TKey> : ModelEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        public NoSqlEntity() { }

        public string Type { get; protected set; }

        public JsonDocument SpecificData { get; set; }
    }

    public class UserAwareNosqlEntity<TKey> : NoSqlEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        public Guid UserId { get; set; }
    }
}
