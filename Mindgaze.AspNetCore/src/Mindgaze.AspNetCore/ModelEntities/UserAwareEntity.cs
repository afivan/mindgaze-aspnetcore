using System;

namespace Mindgaze.AspNetCore.ModelEntities
{
    public class UserAwareEntity<TKey> : ModelEntity<TKey>
        where TKey: IEquatable<TKey>
    {
        public Guid UserId
        {
            get; set;
        }
    }
}