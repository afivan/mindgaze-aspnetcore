﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Testing
{
    public abstract class WebHostTestFixture<TStartup, TDbContext> : IDisposable
        where TStartup : class
        where TDbContext : DbContext
    {

        private readonly CancellationToken CancelToken = new CancellationToken();
        private readonly TDbContext DbContext;

        #region Abstract members

        protected abstract string RelativeProjectPath { get; }

        protected abstract string EnvironmentName { get; }

        #endregion

        /// <summary>
        /// Gets the full path to the target project path that we wish to test
        /// </summary>
        /// <param name="solutionRelativePath">
        /// The parent directory of the target project.
        /// e.g. src, samples, test, or test/Websites
        /// </param>
        /// <param name="startupAssembly">The target project's assembly.</param>
        /// <returns>The full path to the target project.</returns>
        private string GetProjectPath(string solutionRelativePath, Assembly startupAssembly)
        {
            // Get name of the target project which we want to test
            var projectName = startupAssembly.GetName().Name;

            // Get currently executing test project path
            var applicationBasePath = System.AppDomain.CurrentDomain.BaseDirectory;

            // Find the folder which contains the solution file. We then use this information to find the target
            // project which we want to test.
            var directoryInfo = new DirectoryInfo(applicationBasePath);
            do
            {
                var solutionFileInfo = new FileInfo(Path.Combine(directoryInfo.FullName, RelativeProjectPath, $"{projectName}.csproj"));

                if (solutionFileInfo.Exists)
                {
                    //return Path.GetFullPath(Path.Combine(directoryInfo.FullName, solutionRelativePath, projectName));
                    return solutionFileInfo.Directory.FullName;
                }

                directoryInfo = directoryInfo.Parent;
            }
            while (directoryInfo.Parent != null);

            throw new Exception($"Solution root could not be located using application root {applicationBasePath}.");
        }

        protected virtual void ConfigureTestingServices(IServiceCollection services)
        {
        }

        #region Constructors

        protected WebHostTestFixture()
        {
            var contentRoot = GetProjectPath(RelativeProjectPath, typeof(TStartup).GetTypeInfo().Assembly);

            Console.WriteLine($"Using ContentRoot {contentRoot}");

            var baseConfig = new ConfigurationBuilder()
                .AddEnvironmentVariables(prefix: "ASPNETCORE_")
                .Build();

            var config = new ConfigurationBuilder()
                .SetBasePath(contentRoot)
                .AddJsonFile("appsettings.json", optional: false)
                .AddJsonFile($"appsettings.{EnvironmentName}.json", optional: true)
                .AddJsonFile($"appsettings.{baseConfig["ENVIRONMENT"]}.json", optional: true)
                .AddEnvironmentVariables(prefix: "ASPNETCORE_")
                .Build();

            var hostBuilder = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(contentRoot)
                .UseStartup<TStartup>()
                .UseEnvironment(EnvironmentName)
                .UseConfiguration(config)
                .ConfigureTestServices(ConfigureTestingServices)
                ;

            Host = hostBuilder.Build();

            DbContext = GetService<TDbContext>();

            // Make sure the db is clean
            DbContext.Database.EnsureDeleted();
            DbContext.Database.Migrate();

            Task.Run(async () =>
            {
                await Host.RunAsync(CancelToken);
            });

            var listenTo = config["URLS"]
                .Replace("+", "localhost")
                .Replace("*", "localhost")
                ;

            Client = new HttpClient()
            {
                BaseAddress = new Uri(listenTo)
            };
            bool serverStarted = false;
            do
            {

                try
                {
                    Client.GetAsync("/").Wait();
                    serverStarted = true;
                }
                catch (SocketException)
                {
                    Thread.Sleep(1000);
                }
                catch (AggregateException)
                {
                    System.Threading.Thread.Sleep(1000);
                }
                catch (Exception)
                {
                    // This is bad
                    throw;
                }
            }
            while (!serverStarted);
        }

        #endregion

        #region Public members

        public HttpClient Client { get; }
        public IWebHost Host { get; }

        public TService GetService<TService>()
        {
            return (TService)Host.Services.GetService(typeof(TService));
        }

        #endregion

        #region Public methods

        public void Dispose()
        {
            try
            {
                Client.CancelPendingRequests();
                Client.Dispose();
                GC.Collect();

                DbContext.Database.EnsureDeleted();
                Host.StopAsync().Wait();
                Host.Dispose();
                Console.WriteLine("Disposed of resources");
            }
            catch (Exception ex)
            {
                File.AppendAllText("/home/afivan/Documents/asperrors.txt", ex.Message);
                File.AppendAllText("/home/afivan/Documents/asperrors.txt", ex.StackTrace);
                File.AppendAllText("/home/afivan/Documents/asperrors.txt", "\n\n\n");
            }
        }

        #endregion
    }
}
