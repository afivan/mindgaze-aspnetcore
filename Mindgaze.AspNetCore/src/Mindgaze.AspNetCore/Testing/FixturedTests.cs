﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Mindgaze.AspNetCore.Testing
{
    public abstract class FixturedTests<TFixture, TStartup, TDbContext> 
        where TFixture : WebHostTestFixture<TStartup, TDbContext>, new()
        where TStartup : class
        where TDbContext : DbContext
    {
        public abstract string ApiResource { get; }
        public abstract IEnumerable<IDisposable> FixtureDependencies { get; }

        private List<IDisposable> InstantiatedDependentFixtures
        {
            get; set;
        } = new List<IDisposable>();

        protected TFixture TestFixture { get; private set; }

        protected static StringContent GetStringContent(string value)
        {
            return new StringContent(string.Empty, Encoding.UTF8, "application/json");
        }

        protected static StringContent EmptyContent
        {
            get
            {
                return GetStringContent(string.Empty);
            }
        }

        [OneTimeSetUp]
        public virtual void Init()
        {
            TestFixture = new TFixture();

            foreach (var depFixture in FixtureDependencies)
            {
                InstantiatedDependentFixtures.Add(depFixture);
            }
        }

        [OneTimeTearDown]
        public virtual void Cleanup()
        {
            TestFixture.Dispose();

            foreach (var depFixture in InstantiatedDependentFixtures)
            {
                depFixture.Dispose();
            }
        }
        
    }
}
