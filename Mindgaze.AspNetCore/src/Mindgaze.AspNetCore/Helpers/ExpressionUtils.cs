﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Mindgaze.AspNetCore.Helpers
{
    public static class ExpressionUtils
    {
        internal static Expression MultipleAndAlso(params Expression[] expressions)
        {
            Expression resultExpression = null;

            foreach (var expression in expressions)
            {
                if (expression == null)
                {
                    continue;
                }

                if (resultExpression == null)
                {
                    resultExpression = expression;
                }
                else
                {
                    resultExpression = Expression.AndAlso(resultExpression, expression);
                }
            }

            return resultExpression;
        }

        public static Expression<Func<T, bool>> MultipleAndAlso<T>(params Expression<Func<T, bool>>[] expressions)
        {
            Expression resultExpression = null;
            var parameter = Expression.Parameter(typeof(T));

            foreach (var expression in expressions)
            {
                if (expression == null)
                {
                    continue;
                }

                var leftVisitor = new ReplaceExpressionVisitor(expression.Parameters[0], parameter);
                var useExpression = leftVisitor.Visit(expression.Body);

                if (resultExpression == null)
                {
                    resultExpression = useExpression;
                }
                else
                {
                    resultExpression = Expression.AndAlso(resultExpression, useExpression);
                }
            }

            return resultExpression != null ? Expression.Lambda<Func<T, bool>>(resultExpression, parameter) : null;
        }

        private class ReplaceExpressionVisitor
            : ExpressionVisitor
        {
            private readonly Expression _oldValue;
            private readonly Expression _newValue;

            public ReplaceExpressionVisitor(Expression oldValue, Expression newValue)
            {
                _oldValue = oldValue;
                _newValue = newValue;
            }

            public override Expression Visit(Expression node)
            {
                if (node == _oldValue)
                    return _newValue;
                return base.Visit(node);
            }
        }
    }
}
