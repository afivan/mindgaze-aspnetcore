﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Helpers
{
    public static class EntityKeyPropertyStore
    {
        private static readonly ConcurrentDictionary<TypeInfo, PropertyInfo> KeyStore = new ConcurrentDictionary<TypeInfo, PropertyInfo>();

        private static bool HasKeyAttribute(IEnumerable<object> attributes)
        {
            return attributes.Where(o => o.GetType().Equals(typeof(KeyAttribute))).Any();
        }

        private static PropertyInfo FindIdPropertyInfo<TEntity, TKey>()
            where TEntity: class
            where TKey : IEquatable<TKey>
        {
            var typeInfo = typeof(TEntity).GetTypeInfo();

            return typeInfo.GetProperties()
                .Where(propertyInfo => propertyInfo.PropertyType.Equals(typeof(TKey)) && HasKeyAttribute(propertyInfo.GetCustomAttributes(true)))
                .SingleOrDefault();
        }

        private static PropertyInfo RetrieveIdPropertyInfo<TEntity, TKey>()
            where TEntity : class
            where TKey : IEquatable<TKey>
        {
            var typeInfo = typeof(TEntity).GetTypeInfo();
            if (KeyStore.ContainsKey(typeInfo))
            {
                return KeyStore[typeInfo];
            }

            var keyPropertyInfo = FindIdPropertyInfo<TEntity, TKey>();

            if (keyPropertyInfo == null)
            {
                throw new ValidationException("This entity doesn't have a KeyAttribute defined!");
            }

            KeyStore.TryAdd(typeInfo, keyPropertyInfo);

            return keyPropertyInfo;
        }

        public static TKey GetKeyValue<TEntity, TKey>(TEntity entity)
            where TEntity : class
            where TKey : IEquatable<TKey>
        {
            return (TKey)RetrieveIdPropertyInfo<TEntity, TKey>().GetValue(entity);
        }

        public static void SetKeyValue<TEntity, TKey>(TEntity entity, TKey value)
            where TEntity : class
            where TKey : IEquatable<TKey>
        {
            RetrieveIdPropertyInfo<TEntity, TKey>().SetValue(entity, value);
        }

        public static string GetKeyPropertyName<TEntity, TKey>()
            where TEntity : class
            where TKey : IEquatable<TKey>
        {
            return RetrieveIdPropertyInfo<TEntity, TKey>().Name;
        }

        public static string LowercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }

            char[] a = s.ToCharArray();
            a[0] = char.ToLower(a[0]);

            return new string(a);
        }

        public static IEnumerable<string> GetReadOnlyProperties<TDto>()
        {
            var typeInfo = typeof(TDto).GetTypeInfo();
            // TODO Also for subproperties
            return typeInfo.GetProperties()
                .Where(p => p.CustomAttributes.Any(a => a.AttributeType.Equals(typeof(ReadOnlyAttribute)) && (bool)a.ConstructorArguments.Single().Value ))
                .Select(p => p.Name)
                ; 
        }
    }
}
