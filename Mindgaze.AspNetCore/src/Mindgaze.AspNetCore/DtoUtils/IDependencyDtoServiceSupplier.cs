namespace Mindgaze.AspNetCore.DtoUtils
{
    public interface IDependencyDtoServiceSupplier<T1>
    {
        void SupplyServices(T1 val1);
    }

    public interface IDependencyDtoServiceSupplier<T1, T2>
    {
        void SupplyServices(T1 val1, T2 val2);
    }

    public interface IDependencyDtoServiceSupplier<T1, T2, T3>
    {
        void SupplyServices(T1 val1, T2 val2, T3 val3);
    }

    public interface IDependencyDtoServiceSupplier<T1, T2, T3, T4>
    {
        void SupplyServices(T1 val1, T2 val2, T3 val3, T4 val4);
    }
}