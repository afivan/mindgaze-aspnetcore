﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.ActionContexts
{
    public class PagingContext : ContextBase
    {
        public const string PageParam = "page";
        public const string RecordsPerPageParam = "recordsPerPage";

        // private readonly HttpContext Context;
        private readonly int MaximumRecordsPerPage;
        private readonly int DefaultRecordsPerPage;

        // private int ParseQueryParam(string paramName)
        // {
        //     int val;

        //     //if(Context.Request.Query.Any(s => s.Key == paramName) && int.TryParse(Context.Request.Query[paramName], out val))
        //     //{
        //     //    return val;
        //     //}

        //     if (int.TryParse(Context.Request.Query[paramName], out val))
        //     {
        //         return val;
        //     }

        //     return 0;
        // }

        public PagingContext(HttpContext httpContext, int maximumRecordsPerPage = 100, int defaultRecordsPerPage = 50) : base(httpContext)
        {
            // Context = httpContext;
            MaximumRecordsPerPage = maximumRecordsPerPage;
            DefaultRecordsPerPage = defaultRecordsPerPage;

            // Fill values

            Page = ParseQueryParamAsInt32(PageParam) ?? 1;
            RecordsPerPage = ParseQueryParamAsInt32(RecordsPerPageParam) ?? DefaultRecordsPerPage;

            if (Page <= 0)
            {
                Page = 1;
            }

            if (RecordsPerPage > MaximumRecordsPerPage)
            {
                RecordsPerPage = MaximumRecordsPerPage;
            }

            if (RecordsPerPage <= 0)
            {
                RecordsPerPage = DefaultRecordsPerPage;
            }
        }

        public int Page
        {
            get;
            private set;
        }

        public int RecordsPerPage
        {
            get;
            private set;
        }
    }
}
