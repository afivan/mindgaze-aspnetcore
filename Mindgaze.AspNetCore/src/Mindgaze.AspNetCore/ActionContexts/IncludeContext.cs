﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Serialization;

namespace Mindgaze.AspNetCore.ActionContexts
{
    public class IncludeContext : ContextBase
    {
        private static DefaultContractResolver PropertyConvertContractResolver
        {
            get; set;
        } = new CamelCasePropertyNamesContractResolver();
        
        public IncludeContext(HttpContext httpContext) : base(httpContext)
        {
            IncludeProperties = httpContext.Request.Query.Where(x => x.Key == "include").SelectMany(x => x.Value.ToArray());
        }

        public IEnumerable<string> IncludeProperties
        {
            get; private set;
        }
        // Expression<Func<TDestination, object>>
        public Expression<Func<TType, object>> GetIncludedPropertyValue<TType>(string desiredProperty)
            where TType : class
        {
            if (!String.IsNullOrEmpty(desiredProperty))
            {
                var entityType = typeof(TType);
                var argumentExp = Expression.Parameter(entityType);

                var propInfo = entityType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => PropertyConvertContractResolver.GetResolvedPropertyName(p.Name) == desiredProperty).SingleOrDefault();

                if (propInfo != null)
                {
                    var propertyExp = Expression.Property(argumentExp, propInfo);
                    
                    Expression conversion = Expression.Convert(propertyExp, typeof(object));

                    return Expression.Lambda<Func<TType, object>>(conversion, argumentExp);
                }
            }

            return null;
        }

        public IEnumerable<Expression<Func<TType, object>>> GetIncludedPropertyValues<TType>()
            where TType : class
        {
            foreach(var property in IncludeProperties)
            {
                yield return GetIncludedPropertyValue<TType>(property);
            }
        }
    }
}
