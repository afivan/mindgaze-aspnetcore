﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Serialization;
using System.Linq.Expressions;
using System.Reflection;

namespace Mindgaze.AspNetCore.ActionContexts
{
    public sealed class SortingContext : ContextBase
    {
        private static DefaultContractResolver PropertyConvertContractResolver
        {
            get; set;
        } = new CamelCasePropertyNamesContractResolver();

        private readonly string OrderByAscendingString;
        private readonly string OrderByDescendingString;

        private readonly string[] ThenByAscendingValues;
        private readonly string[] ThenByDescendingValues;

        private Expression<Func<TType, object>> GetOrderBy<TType>(string desiredProperty)
            where TType : class
        {
            if (!String.IsNullOrEmpty(desiredProperty))
            {
                var entityType = typeof(TType);
                var argumentExp = Expression.Parameter(entityType);

                var propInfo = entityType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => PropertyConvertContractResolver.GetResolvedPropertyName(p.Name) == desiredProperty).SingleOrDefault();

                if (propInfo != null)
                {
                    var propertyExp = Expression.Property(argumentExp, propInfo);
                    // IComparable could also be used, but I'm not sure if it's better that way
                    Expression conversion = Expression.Convert(propertyExp, typeof(object));

                    return Expression.Lambda<Func<TType, object>>(conversion, argumentExp);
                }
            }

            return null;
        }

        public SortingContext(HttpContext httpContext) : base(httpContext)
        {
            OrderByAscendingString = httpContext.Request.Query["orderByAsc"].FirstOrDefault();
            OrderByDescendingString = httpContext.Request.Query["orderByDesc"].FirstOrDefault();

            ThenByAscendingValues = httpContext.Request.Query["thenByAsc"];
            ThenByDescendingValues = httpContext.Request.Query["thenByDesc"];
        }

        public Expression<Func<TType, object>> GetOrderByAscending<TType>()
            where TType : class
        {
            return GetOrderBy<TType>(OrderByAscendingString);
        }

        public Expression<Func<TType, object>> GetOrderByDescending<TType>()
            where TType : class
        {
            return GetOrderBy<TType>(OrderByDescendingString);
        }

        public IEnumerable<Expression<Func<TType, object>>> GetThenByAscending<TType>()
            where TType : class
        {
            foreach (var thenBy in ThenByAscendingValues)
            {
                yield return GetOrderBy<TType>(thenBy);
            }
        }

        public IEnumerable<Expression<Func<TType, object>>> GetThenByDescending<TType>()
            where TType : class
        {
            foreach (var thenBy in ThenByDescendingValues)
            {
                yield return GetOrderBy<TType>(thenBy);
            }
        }
    }
}
