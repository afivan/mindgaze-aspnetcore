using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.ActionContexts
{
    public class ContextBase
    {
        protected readonly HttpContext Context;

        protected int? ParseQueryParamAsInt32(string paramName)
        {
            int val;

            if (int.TryParse(Context.Request.Query[paramName], out val))
            {
                return val;
            }

            return null;
        }

        protected uint? ParseQueryParamAsUInt32(string paramName)
        {
            uint val;

            if (uint.TryParse(Context.Request.Query[paramName], out val))
            {
                return val;
            }

            return null;
        }

        public ContextBase(HttpContext httpContext)
        {
            Context = httpContext;
        }
    }
}
