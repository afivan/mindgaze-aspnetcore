﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Text.RegularExpressions;
using System.Linq.Expressions;
using System.Reflection;
using Newtonsoft.Json.Serialization;
using Mindgaze.AspNetCore.Helpers;
using Newtonsoft.Json;
using Mindgaze.Utilities.Helpers;
using System.Globalization;

namespace Mindgaze.AspNetCore.ActionContexts
{
    public sealed class FilteringContext : ContextBase
    {
        private static readonly Regex LikeOperationsRegex = new Regex("^(?<property>[\\w\\d]+):(?<value>[^%_]+)$");

        private static DefaultContractResolver PropertyConvertContractResolver
        {
            get; set;
        } = new CamelCasePropertyNamesContractResolver();

        #region Method infos

        private static MethodInfo StringContainsMethodInfo
        {
            get
            {
                var x = default(string);
                return typeof(string).GetMethod(nameof(x.Contains), new Type[] { typeof(string) } );
            }
        }

        private static MethodInfo StringStartsWithMethodInfo
        {
            get
            {
                var x = default(string);
                return typeof(string).GetMethod(nameof(x.StartsWith), new Type[] { typeof(string) });
            }
        }

        private static MethodInfo StringEndsWithMethodInfo
        {
            get
            {
                var x = default(string);
                return typeof(string).GetMethod(nameof(x.EndsWith), new Type[] { typeof(string) });
            }
        }

        #endregion

        #region Private properties

        private Dictionary<string, List<string>> ContainsValues
        {
            get;set;
        }

        private Dictionary<string, List<string>> StartsWithValues
        {
            get; set;
        }

        private Dictionary<string, List<string>> EndsWithValues
        {
            get; set;
        }

        private Dictionary<string, List<string>> EqualsValues
        {
            get; set;
        }

        private Dictionary<string, List<string>> GreaterThanValues
        {
            get; set;
        }

        private Dictionary<string, List<string>> GreaterThanEqualValues
        {
            get; set;
        }

        private Dictionary<string, List<string>> LessThanValues
        {
            get; set;
        }

        private Dictionary<string, List<string>> LessThanEqualValues
        {
            get; set;
        }

        private Dictionary<string, List<string>> NotEqualsValues
        {
            get; set;
        }

        #endregion

        #region Constructor

        public FilteringContext(HttpContext httpContext) : base(httpContext)
        {
            ProcessFilteringContext();
        }

        #endregion

        #region Private methods

        private Dictionary<string, List<string>> ProcessLikeOperationFromQuery(string likeOperation)
        {
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();

            var containsStringValues = Context.Request.Query[likeOperation];

            foreach (var contains in containsStringValues)
            {
                var match = LikeOperationsRegex.Match(contains);

                if (match.Success)
                {
                    var property = match.Groups["property"].Value;
                    var value = match.Groups["value"].Value;

                    if (result.ContainsKey(property))
                    {
                        result[property].Add(value);
                    }
                    else
                    {
                        result.Add(property, new List<string>()
                        {
                            value
                        });
                    }
                }
            }

            return result;
        }

        private void ProcessFilteringContext()
        {
            // ConditionBlock
            // containsOp = OR | AND | XOR
            ContainsValues = ProcessLikeOperationFromQuery("contains");
            StartsWithValues = ProcessLikeOperationFromQuery("starts");
            EndsWithValues = ProcessLikeOperationFromQuery("ends");
            EqualsValues = ProcessLikeOperationFromQuery("eq");
            GreaterThanValues = ProcessLikeOperationFromQuery("gt");
            GreaterThanEqualValues = ProcessLikeOperationFromQuery("gte");
            LessThanValues = ProcessLikeOperationFromQuery("lt");
            LessThanEqualValues = ProcessLikeOperationFromQuery("lte");
            NotEqualsValues = ProcessLikeOperationFromQuery("neq");
        }


        private Expression GetWhereClausePredicate<TType, TProperty>(Dictionary<string, List<string>> valuesDict, ParameterExpression argumentExp,Func<Expression, ConstantExpression, Expression> composePropertyConditionLambda, Func<string, TProperty> converter = null)
            where TType : class
        {
            var entityType = typeof(TType);

            Expression whereClauseExpression = null;

            const BindingFlags retrieveFlags = BindingFlags.Public | BindingFlags.Instance;

            foreach (var containsArg in valuesDict)
            {
                var propInfo = entityType.GetProperties(retrieveFlags).Where(p => PropertyConvertContractResolver.GetResolvedPropertyName(p.Name) == containsArg.Key).SingleOrDefault();

                if (propInfo != null && !propInfo.DeclaringType.Equals(entityType))
                {
                    propInfo = propInfo.DeclaringType.GetProperties(retrieveFlags).Where(p => p.Name == propInfo.Name).SingleOrDefault();
                }

                if (propInfo != null && propInfo.PropertyType.Equals(typeof(TProperty)) && !propInfo.CustomAttributes.Any(attr => attr.AttributeType.Equals(typeof(JsonIgnoreAttribute)))  )
                {
                    var propertyExp = Expression.Property(argumentExp, propInfo);
                    
                    Expression propertyLevelClauseExpression = null;

                    foreach (var value in containsArg.Value)
                    {
                        var constantOperandExp = Expression.Constant(value);

                        if (converter != null)
                        {
                            try
                            {
                                constantOperandExp = Expression.Constant(converter(value));
                            }
                            catch
                            {
                                // TODO Issue warning
                                continue;
                            }
                        }
                        
                        var auxExpression = composePropertyConditionLambda(propertyExp, constantOperandExp);

                        if (propertyLevelClauseExpression == null)
                        {
                            propertyLevelClauseExpression = auxExpression;
                        }
                        else
                        {
                            propertyLevelClauseExpression = Expression.OrElse(propertyLevelClauseExpression, auxExpression);
                        }
                    }

                    if (whereClauseExpression == null)
                    {
                        whereClauseExpression = propertyLevelClauseExpression;
                    }
                    else
                    {
                        whereClauseExpression = Expression.AndAlso(whereClauseExpression, propertyLevelClauseExpression);
                    }
                }

            }

            // if (whereClauseExpression == null)
            // {
            //     whereClauseExpression = Expression.Constant(true);
            // }

            return whereClauseExpression;
        }
        
        #endregion

        public Expression<Func<TType, bool>> GetWhereClausePredicate<TType>()
            where TType : class
        {
            var entityType = typeof(TType);
            var entityTypeInfo = entityType.GetTypeInfo();

            var argumentExp = Expression.Parameter(entityType, "X");

            // String related
            var containsWhereClause = GetWhereClausePredicate<TType, string>(ContainsValues, argumentExp,(propExp, constOp) => Expression.Call(propExp, StringContainsMethodInfo, constOp));
            var startsWhereClause = GetWhereClausePredicate<TType, string>(StartsWithValues, argumentExp,(propExp, constOp) => Expression.Call(propExp, StringStartsWithMethodInfo, constOp));
            var endsWhereClause = GetWhereClausePredicate<TType, string>(EndsWithValues, argumentExp,(propExp, constOp) => Expression.Call(propExp, StringEndsWithMethodInfo, constOp));
            // Equals
            var equalsForStringWhereClause = GetWhereClausePredicate<TType, string>(EqualsValues, argumentExp,(propExp, constOp) => Expression.Equal(propExp, constOp));
            var equalsForByteWhereClause = GetWhereClausePredicate<TType, byte>(EqualsValues, argumentExp,(propExp, constOp) => Expression.Equal(propExp, constOp), (val) => byte.Parse(val));
            var equalsForSByteWhereClause = GetWhereClausePredicate<TType, sbyte>(EqualsValues, argumentExp,(propExp, constOp) => Expression.Equal(propExp, constOp), (val) => sbyte.Parse(val));
            var equalsForShortWhereClause = GetWhereClausePredicate<TType, short>(EqualsValues, argumentExp,(propExp, constOp) => Expression.Equal(propExp, constOp), (val) => short.Parse(val));
            var equalsForUShortWhereClause = GetWhereClausePredicate<TType, ushort>(EqualsValues, argumentExp,(propExp, constOp) => Expression.Equal(propExp, constOp), (val) => ushort.Parse(val));
            var equalsForIntWhereClause = GetWhereClausePredicate<TType, int>(EqualsValues, argumentExp,(propExp, constOp) => Expression.Equal(propExp, constOp), (val) => int.Parse(val));
            var equalsForUIntWhereClause = GetWhereClausePredicate<TType, uint>(EqualsValues, argumentExp,(propExp, constOp) => Expression.Equal(propExp, constOp), (val) => uint.Parse(val));
            var equalsForGuidWhereClause = GetWhereClausePredicate<TType, Guid>(EqualsValues, argumentExp, (propExp, constOp) => Expression.Equal(propExp, constOp), (val) => Guid.Parse(val));
            // There is an issue with float equal might be as well for double
            var equalsForFloatWhereClause = GetWhereClausePredicate<TType, float>(EqualsValues, argumentExp,(propExp, constOp) => Expression.Equal(propExp, constOp), (val) => float.Parse(val));
            var equalsForDoubleWhereClause = GetWhereClausePredicate<TType, double>(EqualsValues, argumentExp,(propExp, constOp) => Expression.Equal(propExp, constOp), (val) => double.Parse(val));
            var equalsForLongWhereClause = GetWhereClausePredicate<TType, long>(EqualsValues, argumentExp,(propExp, constOp) => Expression.Equal(propExp, constOp), (val) => long.Parse(val));
            var equalsForULongWhereClause = GetWhereClausePredicate<TType, ulong>(EqualsValues, argumentExp,(propExp, constOp) => Expression.Equal(propExp, constOp), (val) => ulong.Parse(val));
            var equalsForDateTimeWhereClause = GetWhereClausePredicate<TType, DateTime>(EqualsValues, argumentExp, (propExp, constOp) => Expression.Equal(propExp, constOp), (val) => DateTime.Parse(val, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal));

            // Greater than
            var greaterThanForByteWhereClause = GetWhereClausePredicate<TType, byte>(GreaterThanValues, argumentExp,(propExp, constOp) => Expression.GreaterThan(propExp, constOp), (val) => byte.Parse(val));
            var greaterThanForSByteWhereClause = GetWhereClausePredicate<TType, sbyte>(GreaterThanValues, argumentExp,(propExp, constOp) => Expression.GreaterThan(propExp, constOp), (val) => sbyte.Parse(val));
            var greaterThanForShortWhereClause = GetWhereClausePredicate<TType, short>(GreaterThanValues, argumentExp,(propExp, constOp) => Expression.GreaterThan(propExp, constOp), (val) => short.Parse(val));
            var greaterThanForUShortWhereClause = GetWhereClausePredicate<TType, ushort>(GreaterThanValues, argumentExp,(propExp, constOp) => Expression.GreaterThan(propExp, constOp), (val) => ushort.Parse(val));
            var greaterThanForIntWhereClause = GetWhereClausePredicate<TType, int>(GreaterThanValues, argumentExp,(propExp, constOp) => Expression.GreaterThan(propExp, constOp), (val) => int.Parse(val));
            var greaterThanForUIntWhereClause = GetWhereClausePredicate<TType, uint>(GreaterThanValues, argumentExp,(propExp, constOp) => Expression.GreaterThan(propExp, constOp), (val) => uint.Parse(val));
            var greaterThanForFloatWhereClause = GetWhereClausePredicate<TType, float>(GreaterThanValues, argumentExp,(propExp, constOp) => Expression.GreaterThan(propExp, constOp), (val) => float.Parse(val));
            var greaterThanForDoubleWhereClause = GetWhereClausePredicate<TType, double>(GreaterThanValues, argumentExp,(propExp, constOp) => Expression.GreaterThan(propExp, constOp), (val) => double.Parse(val));
            var greaterThanForLongWhereClause = GetWhereClausePredicate<TType, long>(GreaterThanValues, argumentExp,(propExp, constOp) => Expression.GreaterThan(propExp, constOp), (val) => long.Parse(val));
            var greaterThanForULongWhereClause = GetWhereClausePredicate<TType, ulong>(GreaterThanValues, argumentExp,(propExp, constOp) => Expression.GreaterThan(propExp, constOp), (val) => ulong.Parse(val));
            var greaterThanForDateTimeWhereClause = GetWhereClausePredicate<TType, DateTime>(GreaterThanValues, argumentExp, (propExp, constOp) => Expression.GreaterThan(propExp, constOp), (val) => DateTime.Parse(val, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal));
            // Greater than equal
            var greaterThanEqualForByteWhereClause = GetWhereClausePredicate<TType, byte>(GreaterThanEqualValues, argumentExp,(propExp, constOp) => Expression.GreaterThanOrEqual(propExp, constOp), (val) => byte.Parse(val));
            var greaterThanEqualForSByteWhereClause = GetWhereClausePredicate<TType, sbyte>(GreaterThanEqualValues, argumentExp,(propExp, constOp) => Expression.GreaterThanOrEqual(propExp, constOp), (val) => sbyte.Parse(val));
            var greaterThanEqualForShortWhereClause = GetWhereClausePredicate<TType, short>(GreaterThanEqualValues, argumentExp,(propExp, constOp) => Expression.GreaterThanOrEqual(propExp, constOp), (val) => short.Parse(val));
            var greaterThanEqualForUShortWhereClause = GetWhereClausePredicate<TType, ushort>(GreaterThanEqualValues, argumentExp,(propExp, constOp) => Expression.GreaterThanOrEqual(propExp, constOp), (val) => ushort.Parse(val));
            var greaterThanEqualForIntWhereClause = GetWhereClausePredicate<TType, int>(GreaterThanEqualValues, argumentExp,(propExp, constOp) => Expression.GreaterThanOrEqual(propExp, constOp), (val) => int.Parse(val));
            var greaterThanEqualForUIntWhereClause = GetWhereClausePredicate<TType, uint>(GreaterThanEqualValues, argumentExp,(propExp, constOp) => Expression.GreaterThanOrEqual(propExp, constOp), (val) => uint.Parse(val));
            var greaterThanEqualForFloatWhereClause = GetWhereClausePredicate<TType, float>(GreaterThanEqualValues, argumentExp,(propExp, constOp) => Expression.GreaterThanOrEqual(propExp, constOp), (val) => float.Parse(val));
            var greaterThanEqualForDoubleWhereClause = GetWhereClausePredicate<TType, double>(GreaterThanEqualValues, argumentExp,(propExp, constOp) => Expression.GreaterThanOrEqual(propExp, constOp), (val) => double.Parse(val));
            var greaterThanEqualForLongWhereClause = GetWhereClausePredicate<TType, long>(GreaterThanEqualValues, argumentExp,(propExp, constOp) => Expression.GreaterThanOrEqual(propExp, constOp), (val) => long.Parse(val));
            var greaterThanEqualForULongWhereClause = GetWhereClausePredicate<TType, ulong>(GreaterThanEqualValues, argumentExp,(propExp, constOp) => Expression.GreaterThanOrEqual(propExp, constOp), (val) => ulong.Parse(val));
            var greaterThanEqualForDateTimeWhereClause = GetWhereClausePredicate<TType, DateTime>(GreaterThanEqualValues, argumentExp, (propExp, constOp) => Expression.GreaterThanOrEqual(propExp, constOp), (val) => DateTime.Parse(val, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal));
            
            // Less than
            var lessThanForByteWhereClause = GetWhereClausePredicate<TType, byte>(LessThanValues, argumentExp,(propExp, constOp) => Expression.LessThan(propExp, constOp), (val) => byte.Parse(val));
            var lessThanForSByteWhereClause = GetWhereClausePredicate<TType, sbyte>(LessThanValues, argumentExp,(propExp, constOp) => Expression.LessThan(propExp, constOp), (val) => sbyte.Parse(val));
            var lessThanForShortWhereClause = GetWhereClausePredicate<TType, short>(LessThanValues, argumentExp,(propExp, constOp) => Expression.LessThan(propExp, constOp), (val) => short.Parse(val));
            var lessThanForUShortWhereClause = GetWhereClausePredicate<TType, ushort>(LessThanValues, argumentExp,(propExp, constOp) => Expression.LessThan(propExp, constOp), (val) => ushort.Parse(val));
            var lessThanForIntWhereClause = GetWhereClausePredicate<TType, int>(LessThanValues, argumentExp,(propExp, constOp) => Expression.LessThan(propExp, constOp), (val) => int.Parse(val));
            var lessThanForUIntWhereClause = GetWhereClausePredicate<TType, uint>(LessThanValues, argumentExp,(propExp, constOp) => Expression.LessThan(propExp, constOp), (val) => uint.Parse(val));
            var lessThanForFloatWhereClause = GetWhereClausePredicate<TType, float>(LessThanValues, argumentExp,(propExp, constOp) => Expression.LessThan(propExp, constOp), (val) => float.Parse(val));
            var lessThanForDoubleWhereClause = GetWhereClausePredicate<TType, double>(LessThanValues, argumentExp,(propExp, constOp) => Expression.LessThan(propExp, constOp), (val) => double.Parse(val));
            var lessThanForLongWhereClause = GetWhereClausePredicate<TType, long>(LessThanValues, argumentExp,(propExp, constOp) => Expression.LessThan(propExp, constOp), (val) => long.Parse(val));
            var lessThanForULongWhereClause = GetWhereClausePredicate<TType, ulong>(LessThanValues, argumentExp,(propExp, constOp) => Expression.LessThan(propExp, constOp), (val) => ulong.Parse(val));
            var lessThanForDateTimeWhereClause = GetWhereClausePredicate<TType, DateTime>(LessThanValues, argumentExp, (propExp, constOp) => Expression.LessThan(propExp, constOp), (val) => DateTime.Parse(val, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal));
            // Less than equal
            var lessThanEqualForByteWhereClause = GetWhereClausePredicate<TType, byte>(LessThanEqualValues, argumentExp,(propExp, constOp) => Expression.LessThanOrEqual(propExp, constOp), (val) => byte.Parse(val));
            var lessThanEqualForSByteWhereClause = GetWhereClausePredicate<TType, sbyte>(LessThanEqualValues, argumentExp,(propExp, constOp) => Expression.LessThanOrEqual(propExp, constOp), (val) => sbyte.Parse(val));
            var lessThanEqualForShortWhereClause = GetWhereClausePredicate<TType, short>(LessThanEqualValues, argumentExp,(propExp, constOp) => Expression.LessThanOrEqual(propExp, constOp), (val) => short.Parse(val));
            var lessThanEqualForUShortWhereClause = GetWhereClausePredicate<TType, ushort>(LessThanEqualValues, argumentExp,(propExp, constOp) => Expression.LessThanOrEqual(propExp, constOp), (val) => ushort.Parse(val));
            var lessThanEqualForIntWhereClause = GetWhereClausePredicate<TType, int>(LessThanEqualValues, argumentExp,(propExp, constOp) => Expression.LessThanOrEqual(propExp, constOp), (val) => int.Parse(val));
            var lessThanEqualForUIntWhereClause = GetWhereClausePredicate<TType, uint>(LessThanEqualValues, argumentExp,(propExp, constOp) => Expression.LessThanOrEqual(propExp, constOp), (val) => uint.Parse(val));
            var lessThanEqualForFloatWhereClause = GetWhereClausePredicate<TType, float>(LessThanEqualValues, argumentExp,(propExp, constOp) => Expression.LessThanOrEqual(propExp, constOp), (val) => float.Parse(val));
            var lessThanEqualForDoubleWhereClause = GetWhereClausePredicate<TType, double>(LessThanEqualValues, argumentExp,(propExp, constOp) => Expression.LessThanOrEqual(propExp, constOp), (val) => double.Parse(val));
            var lessThanEqualForLongWhereClause = GetWhereClausePredicate<TType, long>(LessThanEqualValues, argumentExp,(propExp, constOp) => Expression.LessThanOrEqual(propExp, constOp), (val) => long.Parse(val));
            var lessThanEqualForULongWhereClause = GetWhereClausePredicate<TType, ulong>(LessThanEqualValues, argumentExp,(propExp, constOp) => Expression.LessThanOrEqual(propExp, constOp), (val) => ulong.Parse(val));
            var lessThanEqualForDateTimeWhereClause = GetWhereClausePredicate<TType, DateTime>(LessThanEqualValues, argumentExp, (propExp, constOp) => Expression.LessThanOrEqual(propExp, constOp), (val) => DateTime.Parse(val, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal));

            // Not Equals
            var notEqualsForStringWhereClause = GetWhereClausePredicate<TType, string>(NotEqualsValues, argumentExp, (propExp, constOp) => Expression.NotEqual(propExp, constOp));
            var notEqualsForByteWhereClause = GetWhereClausePredicate<TType, byte>(NotEqualsValues, argumentExp, (propExp, constOp) => Expression.NotEqual(propExp, constOp), (val) => byte.Parse(val));
            var notEqualsForSByteWhereClause = GetWhereClausePredicate<TType, sbyte>(NotEqualsValues, argumentExp, (propExp, constOp) => Expression.NotEqual(propExp, constOp), (val) => sbyte.Parse(val));
            var notEqualsForShortWhereClause = GetWhereClausePredicate<TType, short>(NotEqualsValues, argumentExp, (propExp, constOp) => Expression.NotEqual(propExp, constOp), (val) => short.Parse(val));
            var notEqualsForUShortWhereClause = GetWhereClausePredicate<TType, ushort>(NotEqualsValues, argumentExp, (propExp, constOp) => Expression.NotEqual(propExp, constOp), (val) => ushort.Parse(val));
            var notEqualsForIntWhereClause = GetWhereClausePredicate<TType, int>(NotEqualsValues, argumentExp, (propExp, constOp) => Expression.NotEqual(propExp, constOp), (val) => int.Parse(val));
            var notEqualsForUIntWhereClause = GetWhereClausePredicate<TType, uint>(NotEqualsValues, argumentExp, (propExp, constOp) => Expression.NotEqual(propExp, constOp), (val) => uint.Parse(val));
            var notEqualsForGuidWhereClause = GetWhereClausePredicate<TType, Guid>(NotEqualsValues, argumentExp, (propExp, constOp) => Expression.NotEqual(propExp, constOp), (val) => Guid.Parse(val));
            // There is an issue with float equal might be as well for double
            var notEqualsForFloatWhereClause = GetWhereClausePredicate<TType, float>(NotEqualsValues, argumentExp, (propExp, constOp) => Expression.NotEqual(propExp, constOp), (val) => float.Parse(val));
            var notEqualsForDoubleWhereClause = GetWhereClausePredicate<TType, double>(NotEqualsValues, argumentExp, (propExp, constOp) => Expression.NotEqual(propExp, constOp), (val) => double.Parse(val));
            var notEqualsForLongWhereClause = GetWhereClausePredicate<TType, long>(NotEqualsValues, argumentExp, (propExp, constOp) => Expression.NotEqual(propExp, constOp), (val) => long.Parse(val));
            var notEqualsForULongWhereClause = GetWhereClausePredicate<TType, ulong>(NotEqualsValues, argumentExp, (propExp, constOp) => Expression.NotEqual(propExp, constOp), (val) => ulong.Parse(val));
            var notEqualsForDateTimeWhereClause = GetWhereClausePredicate<TType, DateTime>(NotEqualsValues, argumentExp, (propExp, constOp) => Expression.NotEqual(propExp, constOp), (val) => DateTime.Parse(val, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal));

            // Nullable primitives (as of now in beta)
            // var equalsForIntNullableWhereClause = GetWhereClausePredicate<TType, int?>(EqualsValues, argumentExp,(propExp, constOp) => Expression.Equal(propExp, constOp), (val) => 
            // {
            //     if (int.TryParse(val, out var res))
            //     {
            //         return res;
            //     }

            //     return null;
            // });

            var multipleAndExpression = ExpressionUtils.MultipleAndAlso(
                // String related
                containsWhereClause,
                startsWhereClause,
                endsWhereClause,
                // Equals
                equalsForStringWhereClause,
                equalsForByteWhereClause,
                equalsForSByteWhereClause,
                equalsForShortWhereClause,
                equalsForUShortWhereClause,
                equalsForIntWhereClause,
                equalsForUIntWhereClause,
                equalsForFloatWhereClause,
                equalsForDoubleWhereClause,
                equalsForLongWhereClause,
                equalsForULongWhereClause,
                equalsForDateTimeWhereClause,
                equalsForGuidWhereClause,
                // Greater than
                greaterThanForByteWhereClause,
                greaterThanForSByteWhereClause,
                greaterThanForShortWhereClause,
                greaterThanForUShortWhereClause,
                greaterThanForIntWhereClause,
                greaterThanForUIntWhereClause,
                greaterThanForFloatWhereClause,
                greaterThanForDoubleWhereClause,
                greaterThanForLongWhereClause,
                greaterThanForULongWhereClause,
                greaterThanForDateTimeWhereClause,
                // Greater than equal
                greaterThanEqualForByteWhereClause,
                greaterThanEqualForSByteWhereClause,
                greaterThanEqualForShortWhereClause,
                greaterThanEqualForUShortWhereClause,
                greaterThanEqualForIntWhereClause,
                greaterThanEqualForUIntWhereClause,
                greaterThanEqualForFloatWhereClause,
                greaterThanEqualForDoubleWhereClause,
                greaterThanEqualForLongWhereClause,
                greaterThanEqualForULongWhereClause,
                greaterThanEqualForDateTimeWhereClause,
                // Less than
                lessThanForByteWhereClause,
                lessThanForSByteWhereClause,
                lessThanForShortWhereClause,
                lessThanForUShortWhereClause,
                lessThanForIntWhereClause,
                lessThanForUIntWhereClause,
                lessThanForFloatWhereClause,
                lessThanForDoubleWhereClause,
                lessThanForLongWhereClause,
                lessThanForULongWhereClause,
                lessThanForDateTimeWhereClause,
                // Less than equal
                lessThanEqualForByteWhereClause,
                lessThanEqualForSByteWhereClause,
                lessThanEqualForShortWhereClause,
                lessThanEqualForUShortWhereClause,
                lessThanEqualForIntWhereClause,
                lessThanEqualForUIntWhereClause,
                lessThanEqualForFloatWhereClause,
                lessThanEqualForDoubleWhereClause,
                lessThanEqualForLongWhereClause,
                lessThanEqualForULongWhereClause,
                lessThanEqualForDateTimeWhereClause,
                // Not Equals
                notEqualsForStringWhereClause,
                notEqualsForByteWhereClause,
                notEqualsForSByteWhereClause,
                notEqualsForShortWhereClause,
                notEqualsForUShortWhereClause,
                notEqualsForIntWhereClause,
                notEqualsForUIntWhereClause,
                notEqualsForFloatWhereClause,
                notEqualsForDoubleWhereClause,
                notEqualsForLongWhereClause,
                notEqualsForULongWhereClause,
                notEqualsForDateTimeWhereClause,
                notEqualsForGuidWhereClause
                // Nullable
                // equalsForIntNullableWhereClause
            );

            if (multipleAndExpression != null)
            {
                return Expression.Lambda<Func<TType, bool>>(multipleAndExpression, argumentExp);
            }
            else
            {
                return null;
            }
        }
    }
}
