﻿using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Mvc.Controllers;
using Mindgaze.AspNetCore.Controllers;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Microsoft.OpenApi.Models;

namespace Mindgaze.AspNetCore.Swagger
{
    public class ActionContextsOperationFilter : IOperationFilter
    {
        private readonly string[] ApplyToVerbs = new string[] { "get", };

        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var controllerDescriptor = context.ApiDescription.ActionDescriptor as ControllerActionDescriptor;
            
            if (controllerDescriptor != null && 
                typeof(BaseController).GetTypeInfo().IsAssignableFrom(controllerDescriptor.ControllerTypeInfo) &&
                controllerDescriptor.ActionConstraints
                    .OfType<HttpMethodActionConstraint>()
                    .Where(x => x.HttpMethods.Intersect(ApplyToVerbs, StringComparer.CurrentCultureIgnoreCase).Any())
                    .Any()&&
                //!controllerDescriptor.MethodInfo.CustomAttributes.OfType<IgnoreActionContextOperationFilterAttribute>().Any()
                !controllerDescriptor.MethodInfo.CustomAttributes.Any(x => x.AttributeType.Equals(typeof(IgnoreActionContextOperationFilterAttribute)))
                )
            {
                // We've got the controller that has ActionContexts and the method has a GET verb
                operation.Parameters = operation.Parameters ?? (operation.Parameters = new List<OpenApiParameter>());

                #region Filtering context

                operation.Parameters.Add(new OpenApiParameter
                {
                    Description = "Equality constraint. Usage [field]:[value]",
                    Name = "eq",
                });

                operation.Parameters.Add(new OpenApiParameter
                {
                    Description = "Contains constraint. Usage [field]:[value]",
                    Name = "contains",
                });

                operation.Parameters.Add(new OpenApiParameter
                {
                    Description = "Starts constraint. Usage [field]:[value]",
                    Name = "starts",
                });

                operation.Parameters.Add(new OpenApiParameter
                {
                    Description = "Ends constraint. Usage [field]:[value]",
                    Name = "ends",
                });

                operation.Parameters.Add(new OpenApiParameter
                {
                    Description = "Greater than constraint. Usage [field]:[value]",
                    Name = "gt",
                });

                operation.Parameters.Add(new OpenApiParameter
                {
                    Description = "Greater than equal constraint. Usage [field]:[value]",
                    Name = "gte",
                });

                operation.Parameters.Add(new OpenApiParameter
                {
                    Description = "Less than constraint. Usage [field]:[value]",
                    Name = "lt",
                });

                operation.Parameters.Add(new OpenApiParameter
                {
                    Description = "Less than equal constraint. Usage [field]:[value]",
                    Name = "lte",
                });

                #endregion

                #region Sorting context

                operation.Parameters.Add(new OpenApiParameter
                {
                    Description = "Order by ascending",
                    Name = "orderByAsc",
                });

                operation.Parameters.Add(new OpenApiParameter
                {
                    Description = "Order by descending",
                    Name = "orderByDesc",
                });

                #endregion

                #region Paging context

                operation.Parameters.Add(new OpenApiParameter
                {
                    Description = "Page number",
                    Name = "page",
                });

                operation.Parameters.Add(new OpenApiParameter
                {
                    Description = "Records to show per page",
                    Name = "recordsPerPage",
                });

                #endregion

                #region Including context

                operation.Parameters.Add(new OpenApiParameter
                {
                    Description = "What to include",
                    Name = "include",
                });

                #endregion

                // TODO Not working
                //operation.Extensions.Add("x-ms-pageable", new
                //{
                //    nextLinkName = "pager/nextPageUri",
                //    itemName = "items",
                //    operationName = "Next",
                //});
            }
        }
        
    }
}
