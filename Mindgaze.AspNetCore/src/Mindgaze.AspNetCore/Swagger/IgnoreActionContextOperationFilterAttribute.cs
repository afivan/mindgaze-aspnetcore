﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Swagger
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class IgnoreActionContextOperationFilterAttribute:Attribute
    {

    }
}
