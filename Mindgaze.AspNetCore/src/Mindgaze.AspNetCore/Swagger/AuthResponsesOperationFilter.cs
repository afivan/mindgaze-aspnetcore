﻿using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Authorization;
using Microsoft.OpenApi.Models;

namespace Mindgaze.AspNetCore.Swagger
{
    public class AuthResponsesOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (context.ApiDescription.TryGetMethodInfo(out var methodInfo))
            {
                var authAttributes = methodInfo.GetCustomAttributes(true).OfType<AuthorizeAttribute>();

                if (authAttributes.Any())
                {
                    operation.Responses.Add("401", new OpenApiResponse { Description = "Unauthorized" });

                    var roles = authAttributes.Select(a => a.Roles).Aggregate((r1, r2) => $"{r1},{r2}");

                    if (!String.IsNullOrEmpty(roles))
                    {
                        operation.Responses.Add("403", new OpenApiResponse { Description = $"Forbidden. One of the '{roles}' roles required" });
                    }
                }
            }
        }
    }
}
