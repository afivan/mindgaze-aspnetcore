﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Mindgaze.AspNetCore.Extensions;

namespace Mindgaze.AspNetCore.Tests
{
    public class IPAddressExtensionsTests
    {
        private const string Loopback = "127.0.0.1";

        [Test]
        public void SubnetMaskForLoopback_ReturnsOk()
        {
            // Arrange
            var address = IPAddress.Parse(Loopback);

            // Act
            var result = address.GetSubnetMask();

            // Assert
            Assert.That(result != null);
            Assert.That("255.0.0.0" == result.ToString());
        }

        [Test]
        public void CheckLoopbackInterface()
        {
            // Arrange
            var address1 = IPAddress.Parse(Loopback);
            var address2 = IPAddress.Parse(Loopback);

            // Act
            var result = address1.IsInSameSubnet(address2, null);

            // Assert
            Assert.That(result);
        }

        [Test]
        [Sequential]
        public void ExpectInSameSubnet(
            [Values("192.168.0.186", "192.168.0.186", "192.168.0.1", "192.168.2.3", "10.0.0.85", "10.0.0.145")]
            string ip1,
            [Values("192.168.0.186", "192.168.0.54", "192.168.0.1", "192.168.2.42", "10.23.0.85", "10.0.22.145")]
            string ip2,
            [Values("255.255.255.0", "255.255.255.0", "255.255.255.0", "255.0.0.0", "255.0.0.0", "255.0.0.0")]
            string subnet)
        {
            // Arrange
            var address1 = IPAddress.Parse(ip1);
            var address2 = IPAddress.Parse(ip2);
            var subnetMask = IPAddress.Parse(subnet);

            // Act
            var result = address1.IsInSameSubnet(address2, subnetMask);

            // Assert
            Assert.That(result);
        }

        [Test]
        [Sequential]
        public void ExpectNotInSameSubnet(
            [Values("192.168.0.186", "192.168.0.186", "192.168.0.1", "194.168.2.3", "10.0.0.85", "11.0.0.145")]
            string ip1,
            [Values("192.168.2.186", "10.23.0.85", "192.168.23.1", "192.168.2.42", "192.168.0.54", "10.0.22.145")]
            string ip2,
            [Values("255.255.255.0", "255.255.255.0", "255.255.255.0", "255.3.0.0", "255.0.0.0", "255.0.0.0")]
            string subnet)
        {
            // Arrange
            var address1 = IPAddress.Parse(ip1);
            var address2 = IPAddress.Parse(ip2);
            var subnetMask = IPAddress.Parse(subnet);

            // Act
            var result = address1.IsInSameSubnet(address2, subnetMask);

            // Assert
            Assert.That(!result);
        }
    }
}
