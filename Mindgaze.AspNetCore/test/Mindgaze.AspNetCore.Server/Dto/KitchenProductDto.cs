﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Server.Dto
{
    public class KitchenProductDto : ProductDto
    {
        public short PowerLevel { get; set; }
    }
}
