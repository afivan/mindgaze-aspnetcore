﻿using Mindgaze.AspNetCore.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Server.Dto
{
    public class ProductDto : ModelDto<int>
    {
        public string Name { get; set; }

        public float Price { get; set; }

        public uint Quantity { get; set; }
    }
}
