﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Server.Dto
{
    public class BathroomProductDto : ProductDto
    {
        public bool CleansLimescale { get; set; }
    }
}
