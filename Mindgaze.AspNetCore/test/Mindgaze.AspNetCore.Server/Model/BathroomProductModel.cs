﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Server.Model
{
    public class BathroomProductModel : ProductModel
    {
        public bool CleansLimescale { get; set; }
    }
}
