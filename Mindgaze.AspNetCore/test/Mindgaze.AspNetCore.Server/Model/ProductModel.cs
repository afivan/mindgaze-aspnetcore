﻿using Mindgaze.AspNetCore.ModelEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Server.Model
{
    public class ProductModel : ModelEntity<int>
    {
        public string Name { get; set; }

        public float Price { get; set; }

        public uint Quantity { get; set; }
    }
}
