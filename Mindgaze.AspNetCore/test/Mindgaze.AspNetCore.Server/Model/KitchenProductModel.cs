﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Server.Model
{
    public class KitchenProductModel : ProductModel
    {
        public short PowerLevel { get; set; }
    }
}
