﻿using AutoMapper;
using Mindgaze.AspNetCore.Server.Dto;
using Mindgaze.AspNetCore.Server.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Server.AutoMapper
{
    public class DefaultProfile : Profile
    {
        public DefaultProfile()
        {
            CreateMap<ProductDto, ProductModel>()
                .IncludeAllDerived()
                .ReverseMap()
                .IncludeAllDerived()
                ;

            CreateMap<KitchenProductDto, KitchenProductModel>()
                .ReverseMap()
                ;

            CreateMap<BathroomProductDto, BathroomProductModel>()
                .ReverseMap()
                ;
        }
    }
}
