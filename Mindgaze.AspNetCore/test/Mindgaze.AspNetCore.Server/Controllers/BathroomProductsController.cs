﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mindgaze.AspNetCore.Controllers;
using Mindgaze.AspNetCore.Server.Contexts;
using Mindgaze.AspNetCore.Server.Dto;
using Mindgaze.AspNetCore.Server.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Server.Controllers
{
    [Route("[controller]")]
    public class BathroomProductsController : EntityController<BathroomProductModel, int, AspNetCoreDbContext, BathroomProductDto>
    {
        public BathroomProductsController(AspNetCoreDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
        }

        protected override Func<AspNetCoreDbContext, DbSet<BathroomProductModel>> EntityDbSetPropertyFunc => db => db.BathroomProducts;

        protected override Func<BathroomProductModel, int> EntityGetKeyFunc => e => e.Id;

        protected override Action<BathroomProductModel, int> EntitySetKeyAction => (e, key) => e.Id = key;

        protected override Expression<Func<BathroomProductModel, bool>> GenerateKeyEqualityExpression(int key)
        {
            return e => e.Id == key;
        }
    }
}
