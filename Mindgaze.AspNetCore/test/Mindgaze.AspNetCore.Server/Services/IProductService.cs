﻿using Mindgaze.AspNetCore.Server.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Server.Services
{
    public interface IProductService<out TProduct> where TProduct : ProductDto
    {
        IAsyncEnumerable<TProduct> ListProductsAsync();
    }
}
