﻿using Mindgaze.AspNetCore.Server.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Server.Services.Impl
{
    public class KitchenProductService : IProductService<KitchenProductDto>
    {
        public async IAsyncEnumerable<KitchenProductDto> ListProductsAsync()
        {
            // Async calls...
            yield return new KitchenProductDto
            {
                Name = "Domestos",
                PowerLevel = 1,
                Price = 3.44F,
                Quantity = 3
            };
        }
    }
}
