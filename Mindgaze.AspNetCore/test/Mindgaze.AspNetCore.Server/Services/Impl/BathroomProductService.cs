﻿using Mindgaze.AspNetCore.Server.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Server.Services.Impl
{
    public class BathroomProductService : IProductService<BathroomProductDto>
    {
        public async IAsyncEnumerable<BathroomProductDto> ListProductsAsync()
        {
            // Async calls...
            yield return new BathroomProductDto
            {
                Name = "Proper",
                CleansLimescale = false,
                Price = 3.44F,
                Quantity = 3
            };
        }
    }
}
