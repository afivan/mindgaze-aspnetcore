﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mindgaze.AspNetCore.Server.Dto;

namespace Mindgaze.AspNetCore.Server.Services.Impl
{
    public class ProductCenter : IProductCenter
    {
        public IEnumerable<IProductService<ProductDto>> GetServices()
        {
            yield return new KitchenProductService();
            yield return new BathroomProductService();
        }
    }
}
