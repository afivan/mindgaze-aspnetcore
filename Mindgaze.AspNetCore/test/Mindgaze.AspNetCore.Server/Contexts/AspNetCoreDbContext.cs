﻿using Microsoft.EntityFrameworkCore;
using Mindgaze.AspNetCore.Server.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.AspNetCore.Server.Contexts
{
    public class AspNetCoreDbContext : DbContext
    {
        public AspNetCoreDbContext(DbContextOptions<AspNetCoreDbContext> options) : base(options)
        {

        }

        public DbSet<BathroomProductModel> BathroomProducts { get; set; }

        public DbSet<KitchenProductModel> KitchenProducts { get; set; }
    }
}
